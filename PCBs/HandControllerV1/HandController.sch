<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.4.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Standard">
<packages>
<package name="SOT95P280X145-5" urn="urn:adsk.eagle:footprint:11372015/2">
<description>5-SOT23, 0.95 mm pitch, 2.80 mm span, 2.90 X 1.60 X 1.45 mm body
&lt;p&gt;5-pin SOT23 package with 0.95 mm pitch, 2.80 mm span with body size 2.90 X 1.60 X 1.45 mm&lt;/p&gt;</description>
<circle x="-1.379" y="1.7941" radius="0.25" width="0" layer="21"/>
<wire x1="-0.875" y1="1.6041" x2="0.875" y2="1.6041" width="0.12" layer="21" style="shortdash"/>
<wire x1="-0.875" y1="-1.6041" x2="0.875" y2="-1.6041" width="0.12" layer="21"/>
<smd name="IN" x="-1.2533" y="0.95" dx="1.2088" dy="0.6802" layer="1"/>
<smd name="GND" x="-1.2533" y="0" dx="1.2088" dy="0.6802" layer="1"/>
<smd name="EN" x="-1.2533" y="-0.95" dx="1.2088" dy="0.6802" layer="1"/>
<smd name="NC" x="1.2533" y="-0.95" dx="1.2088" dy="0.6802" layer="1"/>
<smd name="OUT" x="1.2533" y="0.95" dx="1.2088" dy="0.6802" layer="1"/>
<text x="0" y="2.6791" size="0.6096" layer="25" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2391" size="0.6096" layer="27" font="vector" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1005X30" urn="urn:adsk.eagle:footprint:11214500/1" locally_modified="yes">
<description>Chip, 1.00 X 0.50 X 0.30 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.30 mm&lt;/p&gt;</description>
<smd name="1" x="-0.5" y="0" dx="0.5118" dy="0.6118" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.5118" dy="0.6118" layer="1"/>
<text x="0" y="0.4929" size="0.6096" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.4929" size="0.6096" layer="27" align="top-center">&gt;VALUE</text>
<polygon width="0.0762" layer="21">
<vertex x="-0.127" y="0.254"/>
<vertex x="-0.127" y="-0.254"/>
<vertex x="0.127" y="-0.254"/>
<vertex x="0.127" y="0.254"/>
</polygon>
</package>
<package name="RESC1005X30" urn="urn:adsk.eagle:footprint:11214493/1" locally_modified="yes">
<description>Chip, 1.00 X 0.50 X 0.30 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.30 mm&lt;/p&gt;</description>
<smd name="1" x="-0.5" y="0" dx="0.5118" dy="0.6118" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.5118" dy="0.6118" layer="1"/>
<text x="0" y="0.4929" size="0.6096" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.4929" size="0.6096" layer="27" align="top-center">&gt;VALUE</text>
<wire x1="-0.127" y1="0.254" x2="-0.127" y2="-0.254" width="0.0762" layer="21"/>
<wire x1="-0.127" y1="-0.254" x2="0.127" y2="-0.254" width="0.0762" layer="21"/>
<wire x1="0.127" y1="-0.254" x2="0.127" y2="0.254" width="0.0762" layer="21"/>
<wire x1="0.127" y1="0.254" x2="-0.127" y2="0.254" width="0.0762" layer="21"/>
</package>
<package name="UFQFPN28" urn="urn:adsk.eagle:footprint:12074858/1">
<smd name="PA5" x="0" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA4" x="-0.5" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA3" x="-1" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA6" x="0.5" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA7" x="1" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA2" x="-1.5" y="-1.975" dx="0.35" dy="0.3" layer="1" rot="R90"/>
<smd name="PB0" x="1.5" y="-1.975" dx="0.35" dy="0.3" layer="1" rot="R90"/>
<smd name="PA1" x="-1.975" y="-1.5" dx="0.35" dy="0.3" layer="1"/>
<smd name="PA0" x="-1.875" y="-1" dx="0.55" dy="0.3" layer="1"/>
<smd name="VDDA" x="-1.875" y="-0.5" dx="0.55" dy="0.3" layer="1"/>
<smd name="!RST" x="-1.875" y="0" dx="0.55" dy="0.3" layer="1"/>
<smd name="OSC_OUT" x="-1.875" y="0.5" dx="0.55" dy="0.3" layer="1"/>
<smd name="OSC_IN" x="-1.875" y="1" dx="0.55" dy="0.3" layer="1"/>
<smd name="BOOT0" x="-1.975" y="1.5" dx="0.35" dy="0.3" layer="1"/>
<smd name="PB7" x="-1.5" y="1.975" dx="0.35" dy="0.3" layer="1" rot="R90"/>
<smd name="PB6" x="-1" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PB5" x="-0.5" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PB4" x="0" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PB3" x="0.5" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA15" x="1" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA14" x="1.5" y="1.975" dx="0.35" dy="0.3" layer="1" rot="R90"/>
<smd name="PA13" x="1.975" y="1.5" dx="0.35" dy="0.3" layer="1" rot="R180"/>
<smd name="PA10" x="1.875" y="1" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="PA9" x="1.875" y="0.5" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="VDDIO2" x="1.875" y="0" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="VDD" x="1.875" y="-0.5" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="VSS" x="1.875" y="-1" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="PB1" x="1.975" y="-1.5" dx="0.35" dy="0.3" layer="1" rot="R180"/>
<wire x1="-2.05" y1="2.05" x2="-2.05" y2="1.8" width="0.0762" layer="21"/>
<wire x1="-2.05" y1="2.05" x2="-1.8" y2="2.05" width="0.0762" layer="21"/>
<wire x1="-2.05" y1="-2.05" x2="-2.05" y2="-1.8" width="0.0762" layer="21"/>
<wire x1="-2.05" y1="-2.05" x2="-1.8" y2="-2.05" width="0.0762" layer="21"/>
<wire x1="2.05" y1="-2.05" x2="1.8" y2="-2.05" width="0.0762" layer="21"/>
<wire x1="2.05" y1="-2.05" x2="2.05" y2="-1.8" width="0.0762" layer="21"/>
<wire x1="2.05" y1="2.05" x2="2.05" y2="1.8" width="0.0762" layer="21"/>
<wire x1="2.05" y1="2.05" x2="1.8" y2="2.05" width="0.0762" layer="21"/>
<circle x="-1.15" y="1.05" radius="0.2" width="0.0762" layer="21"/>
<text x="-1.4" y="-1.35" size="0.6096" layer="21" font="vector">&gt;NAME</text>
<polygon width="0" layer="31">
<vertex x="-1.8" y="1.65"/>
<vertex x="-1.65" y="1.5"/>
<vertex x="-1.65" y="1.35"/>
<vertex x="-1.8" y="1.35"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-1.65" y="1.8"/>
<vertex x="-1.5" y="1.65"/>
<vertex x="-1.35" y="1.65"/>
<vertex x="-1.35" y="1.8"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="1.65" y="1.8"/>
<vertex x="1.5" y="1.65"/>
<vertex x="1.35" y="1.65"/>
<vertex x="1.35" y="1.8"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="1.8" y="1.65"/>
<vertex x="1.65" y="1.5"/>
<vertex x="1.65" y="1.35"/>
<vertex x="1.8" y="1.35"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-1.65" y="-1.8"/>
<vertex x="-1.5" y="-1.65"/>
<vertex x="-1.35" y="-1.65"/>
<vertex x="-1.35" y="-1.8"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-1.8" y="-1.65"/>
<vertex x="-1.65" y="-1.5"/>
<vertex x="-1.65" y="-1.35"/>
<vertex x="-1.8" y="-1.35"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="1.8" y="-1.65"/>
<vertex x="1.65" y="-1.5"/>
<vertex x="1.65" y="-1.35"/>
<vertex x="1.8" y="-1.35"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="1.65" y="-1.8"/>
<vertex x="1.5" y="-1.65"/>
<vertex x="1.35" y="-1.65"/>
<vertex x="1.35" y="-1.8"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-1.8381" y="1.7262"/>
<vertex x="-1.6246" y="1.5127"/>
<vertex x="-1.6246" y="1.3119"/>
<vertex x="-1.8381" y="1.3119"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-1.7389" y="1.8254"/>
<vertex x="-1.5254" y="1.6119"/>
<vertex x="-1.3246" y="1.6119"/>
<vertex x="-1.3246" y="1.8254"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-1.7262" y="-1.8127"/>
<vertex x="-1.5127" y="-1.5992"/>
<vertex x="-1.3119" y="-1.5992"/>
<vertex x="-1.3119" y="-1.8127"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-1.8254" y="-1.7135"/>
<vertex x="-1.6119" y="-1.5"/>
<vertex x="-1.6119" y="-1.2992"/>
<vertex x="-1.8254" y="-1.2992"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="1.8254" y="-1.7135"/>
<vertex x="1.6119" y="-1.5"/>
<vertex x="1.6119" y="-1.2992"/>
<vertex x="1.8254" y="-1.2992"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="1.7262" y="-1.8127"/>
<vertex x="1.5127" y="-1.5992"/>
<vertex x="1.3119" y="-1.5992"/>
<vertex x="1.3119" y="-1.8127"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="1.7262" y="1.8381"/>
<vertex x="1.5127" y="1.6246"/>
<vertex x="1.3119" y="1.6246"/>
<vertex x="1.3119" y="1.8381"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="1.8254" y="1.7389"/>
<vertex x="1.6119" y="1.5254"/>
<vertex x="1.6119" y="1.3246"/>
<vertex x="1.8254" y="1.3246"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="-2.15" y="-1.35"/>
<vertex x="-1.65" y="-1.35"/>
<vertex x="-1.65" y="-1.5"/>
<vertex x="-1.8" y="-1.65"/>
<vertex x="-2.15" y="-1.65"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="-1.65" y="-1.8"/>
<vertex x="-1.5" y="-1.65"/>
<vertex x="-1.35" y="-1.65"/>
<vertex x="-1.35" y="-2.15"/>
<vertex x="-1.65" y="-2.15"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="1.35" y="-2.15"/>
<vertex x="1.35" y="-1.65"/>
<vertex x="1.5" y="-1.65"/>
<vertex x="1.65" y="-1.8"/>
<vertex x="1.65" y="-2.15"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="1.8" y="-1.65"/>
<vertex x="1.65" y="-1.5"/>
<vertex x="1.65" y="-1.35"/>
<vertex x="2.15" y="-1.35"/>
<vertex x="2.15" y="-1.65"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="-1.35" y="2.15"/>
<vertex x="-1.35" y="1.65"/>
<vertex x="-1.5" y="1.65"/>
<vertex x="-1.65" y="1.8"/>
<vertex x="-1.65" y="2.15"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="-1.8" y="1.65"/>
<vertex x="-1.65" y="1.5"/>
<vertex x="-1.65" y="1.35"/>
<vertex x="-2.15" y="1.35"/>
<vertex x="-2.15" y="1.65"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="2.15" y="1.35"/>
<vertex x="1.65" y="1.35"/>
<vertex x="1.65" y="1.5"/>
<vertex x="1.8" y="1.65"/>
<vertex x="2.15" y="1.65"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="1.65" y="1.8"/>
<vertex x="1.5" y="1.65"/>
<vertex x="1.35" y="1.65"/>
<vertex x="1.35" y="2.15"/>
<vertex x="1.65" y="2.15"/>
</polygon>
</package>
<package name="SON65P300X300X90-9T170X250" urn="urn:adsk.eagle:footprint:11209461/2">
<description>8-SON, 0.65 mm pitch, 3.00 X 3.00 X 0.90 mm body, 2.50 X 1.70 mm thermal pad
&lt;p&gt;8-pin SON package with 0.65 mm pitch with body size 3.00 X 3.00 X 0.90 mm and thermal pad size 2.50 X 1.70 mm&lt;/p&gt;</description>
<circle x="-2.004" y="1.639" radius="0.25" width="0" layer="21"/>
<wire x1="-1.5" y1="1.389" x2="-1.5" y2="1.564" width="0.12" layer="21"/>
<wire x1="-1.5" y1="1.564" x2="1.5" y2="1.564" width="0.12" layer="21"/>
<wire x1="1.5" y1="1.564" x2="1.5" y2="1.389" width="0.12" layer="21"/>
<wire x1="-1.5" y1="-1.389" x2="-1.5" y2="-1.564" width="0.12" layer="21"/>
<wire x1="-1.5" y1="-1.564" x2="1.5" y2="-1.564" width="0.12" layer="21"/>
<wire x1="1.5" y1="-1.564" x2="1.5" y2="-1.389" width="0.12" layer="21"/>
<smd name="1" x="-1.4506" y="0.975" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="2" x="-1.4506" y="0.325" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="3" x="-1.4506" y="-0.325" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="4" x="-1.4506" y="-0.975" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="5" x="1.4506" y="-0.975" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="6" x="1.4506" y="-0.325" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="7" x="1.4506" y="0.325" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="8" x="1.4506" y="0.975" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="9" x="0" y="0" dx="1.7" dy="2.5" layer="1" thermals="no"/>
<text x="0" y="1.889" size="0.6096" layer="25" font="vector" align="bottom-center">&gt;NAME</text>
</package>
<package name="JST6" urn="urn:adsk.eagle:footprint:12077856/1">
<pad name="4" x="0.625" y="0" drill="0.4"/>
<pad name="3" x="-0.625" y="0" drill="0.4"/>
<pad name="2" x="-1.875" y="0" drill="0.4"/>
<pad name="1" x="-3.125" y="0" drill="0.4"/>
<pad name="5" x="1.875" y="0" drill="0.4"/>
<pad name="6" x="3.125" y="0" drill="0.4"/>
<wire x1="-4.625" y1="0.625" x2="-4.0625" y2="0.625" width="0.127" layer="21"/>
<wire x1="-4.0625" y1="0.625" x2="4.0625" y2="0.625" width="0.127" layer="21"/>
<wire x1="4.0625" y1="0.625" x2="4.625" y2="0.625" width="0.127" layer="21"/>
<wire x1="4.625" y1="0.625" x2="4.625" y2="-4.65" width="0.127" layer="21"/>
<wire x1="4.625" y1="-4.65" x2="4.0625" y2="-4.65" width="0.127" layer="21"/>
<wire x1="4.0625" y1="-4.65" x2="-4.0625" y2="-4.65" width="0.127" layer="21"/>
<wire x1="-4.0625" y1="-4.65" x2="-4.625" y2="-4.65" width="0.127" layer="21"/>
<wire x1="-4.625" y1="-4.65" x2="-4.625" y2="0.625" width="0.127" layer="21"/>
<wire x1="-3.4375" y1="-0.625" x2="-3.4375" y2="-3.75" width="0.127" layer="21"/>
<wire x1="-3.4375" y1="-3.75" x2="-3.125" y2="-4.0625" width="0.127" layer="21"/>
<wire x1="-3.125" y1="-4.0625" x2="-2.8125" y2="-3.75" width="0.127" layer="21"/>
<wire x1="-2.8125" y1="-3.75" x2="-2.8125" y2="-0.625" width="0.127" layer="21"/>
<wire x1="-2.1875" y1="-0.625" x2="-2.1875" y2="-3.75" width="0.127" layer="21"/>
<wire x1="-2.1875" y1="-3.75" x2="-1.875" y2="-4.0625" width="0.127" layer="21"/>
<wire x1="-1.875" y1="-4.0625" x2="-1.5625" y2="-3.75" width="0.127" layer="21"/>
<wire x1="-1.5625" y1="-3.75" x2="-1.5625" y2="-0.625" width="0.127" layer="21"/>
<wire x1="-0.9375" y1="-0.625" x2="-0.9375" y2="-3.75" width="0.127" layer="21"/>
<wire x1="-0.9375" y1="-3.75" x2="-0.625" y2="-4.0625" width="0.127" layer="21"/>
<wire x1="-0.625" y1="-4.0625" x2="-0.3125" y2="-3.75" width="0.127" layer="21"/>
<wire x1="-0.3125" y1="-3.75" x2="-0.3125" y2="-0.625" width="0.127" layer="21"/>
<wire x1="0.3125" y1="-0.625" x2="0.3125" y2="-3.75" width="0.127" layer="21"/>
<wire x1="0.3125" y1="-3.75" x2="0.625" y2="-4.0625" width="0.127" layer="21"/>
<wire x1="0.625" y1="-4.0625" x2="0.9375" y2="-3.75" width="0.127" layer="21"/>
<wire x1="0.9375" y1="-3.75" x2="0.9375" y2="-0.625" width="0.127" layer="21"/>
<wire x1="1.5625" y1="-0.625" x2="1.5625" y2="-3.75" width="0.127" layer="21"/>
<wire x1="1.5625" y1="-3.75" x2="1.875" y2="-4.0625" width="0.127" layer="21"/>
<wire x1="1.875" y1="-4.0625" x2="2.1875" y2="-3.75" width="0.127" layer="21"/>
<wire x1="2.1875" y1="-3.75" x2="2.1875" y2="-0.625" width="0.127" layer="21"/>
<wire x1="2.8125" y1="-0.625" x2="2.8125" y2="-3.75" width="0.127" layer="21"/>
<wire x1="2.8125" y1="-3.75" x2="3.125" y2="-4.0625" width="0.127" layer="21"/>
<wire x1="3.125" y1="-4.0625" x2="3.4375" y2="-3.75" width="0.127" layer="21"/>
<wire x1="3.4375" y1="-3.75" x2="3.4375" y2="-0.625" width="0.127" layer="21"/>
<wire x1="-4.0625" y1="-4.65" x2="-4.0625" y2="-2.8125" width="0.127" layer="21"/>
<wire x1="-4.0625" y1="-2.8125" x2="-4.375" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4.375" y1="-2.5" x2="-4.375" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.375" y1="-1.25" x2="-4.0625" y2="-0.9375" width="0.127" layer="21"/>
<wire x1="-4.0625" y1="-0.9375" x2="-4.0625" y2="0.625" width="0.127" layer="21"/>
<wire x1="4.0625" y1="-4.65" x2="4.0625" y2="-2.8125" width="0.127" layer="21"/>
<wire x1="4.0625" y1="-2.8125" x2="4.375" y2="-2.5" width="0.127" layer="21"/>
<wire x1="4.375" y1="-2.5" x2="4.375" y2="-1.25" width="0.127" layer="21"/>
<wire x1="4.375" y1="-1.25" x2="4.0625" y2="-0.9375" width="0.127" layer="21"/>
<wire x1="4.0625" y1="-0.9375" x2="4.0625" y2="0.625" width="0.127" layer="21"/>
<polygon width="0" layer="21">
<vertex x="-4.6875" y="0.625"/>
<vertex x="-4.0625" y="0.625"/>
<vertex x="-4.0625" y="-0.9375"/>
<vertex x="-4.375" y="-1.25"/>
<vertex x="-4.375" y="-2.5"/>
<vertex x="-4.0625" y="-2.8125"/>
<vertex x="-4.0625" y="-4.65"/>
<vertex x="-4.6875" y="-4.6875"/>
</polygon>
<polygon width="0" layer="21">
<vertex x="4.6875" y="-4.6875"/>
<vertex x="4.6875" y="0.625"/>
<vertex x="4.0625" y="0.625"/>
<vertex x="4.0625" y="-0.9375"/>
<vertex x="4.375" y="-1.25"/>
<vertex x="4.375" y="-2.5"/>
<vertex x="4.0625" y="-2.8125"/>
<vertex x="4.0625" y="-4.65"/>
</polygon>
<text x="-5.08" y="1.27" size="0.6096" layer="21" font="vector">&gt;NAME</text>
</package>
<package name="SWD" urn="urn:adsk.eagle:footprint:12074857/1">
<text x="-1.27" y="5.08" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="1.27" y="2.54" size="0.6096" layer="21" font="vector" align="center">+</text>
<text x="0.9525" y="0.9525" size="0.6096" layer="21" font="vector">SWC</text>
<text x="0.9525" y="-0.3175" size="0.6096" layer="21" font="vector">SWD</text>
<text x="1.27" y="-1.27" size="0.6096" layer="21" font="vector" align="center">-</text>
<text x="1.5875" y="-2.54" size="0.6096" layer="21" font="vector" align="center">!RST</text>
<pad name="VCC" x="0" y="2.54" drill="0.5"/>
<pad name="RST" x="0" y="-2.54" drill="0.5"/>
<pad name="SWC" x="0" y="1.27" drill="0.5"/>
<pad name="SWD" x="0" y="0" drill="0.5"/>
<pad name="VSS" x="0" y="-1.27" drill="0.5"/>
</package>
</packages>
<packages3d>
<package3d name="SOT95P280X145-5" urn="urn:adsk.eagle:package:11372008/2" type="model">
<description>5-SOT23, 0.95 mm pitch, 2.80 mm span, 2.90 X 1.60 X 1.45 mm body
&lt;p&gt;5-pin SOT23 package with 0.95 mm pitch, 2.80 mm span with body size 2.90 X 1.60 X 1.45 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT95P280X145-5"/>
</packageinstances>
</package3d>
<package3d name="CAPC1005X30" urn="urn:adsk.eagle:package:11214499/1" locally_modified="yes" type="model">
<description>Chip, 1.00 X 0.50 X 0.30 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.30 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1005X30"/>
</packageinstances>
</package3d>
<package3d name="RESC1005X30" urn="urn:adsk.eagle:package:11214492/1" locally_modified="yes" type="model">
<description>Chip, 1.00 X 0.50 X 0.30 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.30 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1005X30"/>
</packageinstances>
</package3d>
<package3d name="UFQFPN28" urn="urn:adsk.eagle:package:12074869/2" type="model">
<packageinstances>
<packageinstance name="UFQFPN28"/>
</packageinstances>
</package3d>
<package3d name="SON65P300X300X90-9T170X250" urn="urn:adsk.eagle:package:11209438/2" type="model">
<description>8-SON, 0.65 mm pitch, 3.00 X 3.00 X 0.90 mm body, 2.50 X 1.70 mm thermal pad
&lt;p&gt;8-pin SON package with 0.65 mm pitch with body size 3.00 X 3.00 X 0.90 mm and thermal pad size 2.50 X 1.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SON65P300X300X90-9T170X250"/>
</packageinstances>
</package3d>
<package3d name="JST6" urn="urn:adsk.eagle:package:12077888/1" type="box">
<packageinstances>
<packageinstance name="JST6"/>
</packageinstances>
</package3d>
<package3d name="SWD" urn="urn:adsk.eagle:package:12074868/1" type="box">
<packageinstances>
<packageinstance name="SWD"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="203.835" y2="24.13" width="0.1016" layer="94"/>
<wire x1="203.835" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="203.835" y2="8.89" width="0.1016" layer="94"/>
<wire x1="203.835" y1="8.89" x2="203.835" y2="3.81" width="0.1016" layer="94"/>
<wire x1="203.835" y1="8.89" x2="203.835" y2="13.97" width="0.1016" layer="94"/>
<wire x1="203.835" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="203.835" y1="13.97" x2="203.835" y2="19.05" width="0.1016" layer="94"/>
<wire x1="203.835" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="203.835" y1="19.05" x2="203.835" y2="24.13" width="0.1016" layer="94"/>
<text x="205.74" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="205.74" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="219.075" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="205.486" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
<text x="205.74" y="20.32" size="2.54" layer="94" font="vector">Moritz Geier</text>
</symbol>
<symbol name="UREG" urn="urn:adsk.eagle:symbol:12077984/1">
<pin name="IN" x="-7.62" y="0" visible="off" length="short"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<pin name="GND" x="2.54" y="-5.08" visible="off" length="short" rot="R90"/>
<pin name="EN" x="-2.54" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<text x="-5.08" y="3.81" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="0" size="0.8128" layer="97" font="vector">IN</text>
<text x="3.81" y="0" size="0.8128" layer="97" font="vector" align="bottom-right">OUT</text>
<text x="2.54" y="-1.27" size="0.8128" layer="97" font="vector" align="center">GND</text>
<text x="-2.54" y="-1.27" size="0.8128" layer="97" font="vector" align="center">EN</text>
<text x="5.08" y="-5.08" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="C">
<pin name="P$1" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="P$2" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="-1.905" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="1.905" y2="0.635" width="0.3048" layer="94"/>
<text x="2.54" y="-1.905" size="0.8128" layer="95" rot="R90" align="top-left">&gt;NAME</text>
<text x="-2.54" y="-1.905" size="0.8128" layer="95" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="R">
<wire x1="-2.54" y1="-0.9525" x2="-2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.9525" x2="2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.9525" x2="-2.54" y2="-0.9525" width="0.1524" layer="94"/>
<pin name="P$1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="-2.54" y="1.27" size="0.8128" layer="95">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="0.8128" layer="95" align="top-left">&gt;VALUE</text>
</symbol>
<symbol name="STM32F04_AF">
<pin name="!RST" x="5.08" y="-5.08" length="short" rot="R180"/>
<pin name="OSC_OUT" x="5.08" y="-2.54" length="short" rot="R180"/>
<pin name="BOOT0" x="5.08" y="2.54" length="short" rot="R180"/>
<pin name="OSC_IN" x="5.08" y="0" length="short" rot="R180"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="-11.43" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-11.43" y1="-6.35" x2="-11.43" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-11.43" y1="3.81" x2="2.54" y2="3.81" width="0.1524" layer="94"/>
<text x="-11.43" y="5.08" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="STM32F04_PA">
<pin name="PA0" x="5.08" y="15.24" length="short" rot="R180"/>
<pin name="PA1" x="5.08" y="12.7" length="short" rot="R180"/>
<pin name="PA2" x="5.08" y="10.16" length="short" rot="R180"/>
<pin name="PA3" x="5.08" y="7.62" length="short" rot="R180"/>
<pin name="PA4" x="5.08" y="5.08" length="short" rot="R180"/>
<pin name="PA5" x="5.08" y="2.54" length="short" rot="R180"/>
<pin name="PA6" x="5.08" y="0" length="short" rot="R180"/>
<pin name="PA7" x="5.08" y="-2.54" length="short" rot="R180"/>
<pin name="PA9" x="5.08" y="-5.08" length="short" rot="R180"/>
<pin name="PA10" x="5.08" y="-7.62" length="short" rot="R180"/>
<pin name="PA13" x="5.08" y="-10.16" length="short" rot="R180"/>
<pin name="PA14" x="5.08" y="-12.7" length="short" rot="R180"/>
<pin name="PA15" x="5.08" y="-15.24" length="short" rot="R180"/>
<wire x1="2.54" y1="16.51" x2="2.54" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-16.51" x2="-6.35" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-16.51" x2="-6.35" y2="16.51" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="16.51" x2="2.54" y2="16.51" width="0.1524" layer="94"/>
<text x="-6.35" y="17.78" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="STM32F04_PB">
<wire x1="2.54" y1="8.89" x2="2.54" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-8.89" x2="-6.35" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-8.89" x2="-6.35" y2="8.89" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="8.89" x2="2.54" y2="8.89" width="0.1524" layer="94"/>
<text x="-6.35" y="10.16" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<pin name="PB0" x="5.08" y="7.62" length="short" rot="R180"/>
<pin name="PB1" x="5.08" y="5.08" length="short" rot="R180"/>
<pin name="PB3" x="5.08" y="2.54" length="short" rot="R180"/>
<pin name="PB4" x="5.08" y="0" length="short" rot="R180"/>
<pin name="PB5" x="5.08" y="-2.54" length="short" rot="R180"/>
<pin name="PB6" x="5.08" y="-5.08" length="short" rot="R180"/>
<pin name="PB7" x="5.08" y="-7.62" length="short" rot="R180"/>
</symbol>
<symbol name="STM32F04_PWR">
<pin name="VDD" x="0" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="VDDIO2" x="5.08" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="VDDA" x="-5.08" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="VSS" x="0" y="-7.62" visible="off" length="short" direction="pwr" rot="R90"/>
<circle x="-5.08" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="0" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="5.08" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="0" y="-3.048" radius="1.016" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="4.064" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="4.064" width="0.1524" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="4.064" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-4.064" width="0.1524" layer="94"/>
<text x="-0.635" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VDD</text>
<text x="4.445" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VDDIO2</text>
<text x="-5.715" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VDDA</text>
<text x="-0.635" y="-5.08" size="0.8128" layer="95" font="vector" rot="R90" align="bottom-right">VSS</text>
</symbol>
<symbol name="ATA6561">
<pin name="STBY" x="-10.16" y="5.08" length="short" direction="in"/>
<pin name="TXD" x="-10.16" y="-2.54" length="short" direction="in"/>
<pin name="RXD" x="-10.16" y="-5.08" length="short" direction="out"/>
<pin name="CANH" x="10.16" y="5.08" length="short" direction="pas" rot="R180"/>
<pin name="CANL" x="10.16" y="-5.08" length="short" direction="pas" rot="R180"/>
<wire x1="-7.62" y1="6.35" x2="-7.62" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-6.35" x2="7.62" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-6.35" x2="7.62" y2="6.35" width="0.1524" layer="94"/>
<wire x1="7.62" y1="6.35" x2="-7.62" y2="6.35" width="0.1524" layer="94"/>
<text x="-7.62" y="7.62" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="ATA6561_PWR">
<pin name="VDD" x="2.54" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="VSS" x="0" y="-7.62" visible="off" length="short" direction="pwr" rot="R90"/>
<pin name="VDD1" x="-2.54" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<circle x="2.54" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="0" y="-3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="-2.54" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="4.064" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-4.064" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="4.064" width="0.1524" layer="94"/>
<text x="1.905" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VDD</text>
<text x="-0.635" y="-5.08" size="0.8128" layer="95" font="vector" rot="R90" align="bottom-right">VSS</text>
<text x="-2.54" y="0" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<text x="-3.175" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VIO</text>
</symbol>
<symbol name="JST6">
<pin name="3" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="2" x="5.08" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="1" x="5.08" y="5.08" visible="off" length="short" rot="R180"/>
<pin name="4" x="5.08" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="5" x="5.08" y="-5.08" visible="off" length="short" rot="R180"/>
<pin name="6" x="5.08" y="-7.62" visible="off" length="short" rot="R180"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="-8.89" width="0.254" layer="94"/>
<wire x1="2.54" y1="-8.89" x2="-2.54" y2="-8.89" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-8.89" x2="-2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="SWD">
<pin name="SWD" x="5.08" y="0" visible="pin" length="short" rot="R180"/>
<pin name="SWCLK" x="5.08" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="VCC" x="5.08" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="GND" x="5.08" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="!RST" x="5.08" y="-5.08" visible="pin" length="short" rot="R180"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="-7.62" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-6.35" x2="-7.62" y2="6.35" width="0.254" layer="94"/>
<wire x1="-7.62" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<text x="-7.62" y="7.62" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UREG" urn="urn:adsk.eagle:component:12078000/1" prefix="UREG" uservalue="yes">
<gates>
<gate name="G$1" symbol="UREG" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P280X145-5">
<connects>
<connect gate="G$1" pin="EN" pad="EN"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="IN" pad="IN"/>
<connect gate="G$1" pin="OUT" pad="OUT"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11372008/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X30">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11214499/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="RESC1005X30">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11214492/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32F042G6" prefix="MCU">
<gates>
<gate name="PF" symbol="STM32F04_AF" x="-15.24" y="12.7"/>
<gate name="PA" symbol="STM32F04_PA" x="-20.32" y="-17.78"/>
<gate name="PB" symbol="STM32F04_PB" x="2.54" y="-10.16"/>
<gate name="PWR" symbol="STM32F04_PWR" x="27.94" y="10.16"/>
</gates>
<devices>
<device name="" package="UFQFPN28">
<connects>
<connect gate="PA" pin="PA0" pad="PA0"/>
<connect gate="PA" pin="PA1" pad="PA1"/>
<connect gate="PA" pin="PA10" pad="PA10"/>
<connect gate="PA" pin="PA13" pad="PA13"/>
<connect gate="PA" pin="PA14" pad="PA14"/>
<connect gate="PA" pin="PA15" pad="PA15"/>
<connect gate="PA" pin="PA2" pad="PA2"/>
<connect gate="PA" pin="PA3" pad="PA3"/>
<connect gate="PA" pin="PA4" pad="PA4"/>
<connect gate="PA" pin="PA5" pad="PA5"/>
<connect gate="PA" pin="PA6" pad="PA6"/>
<connect gate="PA" pin="PA7" pad="PA7"/>
<connect gate="PA" pin="PA9" pad="PA9"/>
<connect gate="PB" pin="PB0" pad="PB0"/>
<connect gate="PB" pin="PB1" pad="PB1"/>
<connect gate="PB" pin="PB3" pad="PB3"/>
<connect gate="PB" pin="PB4" pad="PB4"/>
<connect gate="PB" pin="PB5" pad="PB5"/>
<connect gate="PB" pin="PB6" pad="PB6"/>
<connect gate="PB" pin="PB7" pad="PB7"/>
<connect gate="PF" pin="!RST" pad="!RST"/>
<connect gate="PF" pin="BOOT0" pad="BOOT0"/>
<connect gate="PF" pin="OSC_IN" pad="OSC_IN"/>
<connect gate="PF" pin="OSC_OUT" pad="OSC_OUT"/>
<connect gate="PWR" pin="VDD" pad="VDD"/>
<connect gate="PWR" pin="VDDA" pad="VDDA"/>
<connect gate="PWR" pin="VDDIO2" pad="VDDIO2"/>
<connect gate="PWR" pin="VSS" pad="VSS"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12074869/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATA6561" prefix="IC">
<gates>
<gate name="ATA6561" symbol="ATA6561" x="-22.86" y="-7.62"/>
<gate name="PWR" symbol="ATA6561_PWR" x="-2.54" y="-7.62"/>
</gates>
<devices>
<device name="" package="SON65P300X300X90-9T170X250">
<connects>
<connect gate="ATA6561" pin="CANH" pad="7"/>
<connect gate="ATA6561" pin="CANL" pad="6"/>
<connect gate="ATA6561" pin="RXD" pad="4"/>
<connect gate="ATA6561" pin="STBY" pad="8"/>
<connect gate="ATA6561" pin="TXD" pad="1"/>
<connect gate="PWR" pin="VDD" pad="3"/>
<connect gate="PWR" pin="VDD1" pad="5"/>
<connect gate="PWR" pin="VSS" pad="2 9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11209438/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST6" prefix="X">
<gates>
<gate name="G$1" symbol="JST6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JST6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077888/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWD" prefix="SWD">
<gates>
<gate name="G$1" symbol="SWD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SWD">
<connects>
<connect gate="G$1" pin="!RST" pad="RST"/>
<connect gate="G$1" pin="GND" pad="VSS"/>
<connect gate="G$1" pin="SWCLK" pad="SWC"/>
<connect gate="G$1" pin="SWD" pad="SWD"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12074868/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="Standard" deviceset="A4L-LOC" device=""/>
<part name="UREG2" library="Standard" deviceset="UREG" device="" package3d_urn="urn:adsk.eagle:package:11372008/2" value="3V3"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C1" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="1µF"/>
<part name="C2" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="1µF"/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C3" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="100nF"/>
<part name="R1" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/1" value="10k"/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C4" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="100nF"/>
<part name="C5" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="100nF"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="MCU1" library="Standard" deviceset="STM32F042G6" device="" package3d_urn="urn:adsk.eagle:package:12074869/2"/>
<part name="IC1" library="Standard" deviceset="ATA6561" device="" package3d_urn="urn:adsk.eagle:package:11209438/2"/>
<part name="SWD1" library="Standard" deviceset="SWD" device="" package3d_urn="urn:adsk.eagle:package:12074868/1"/>
<part name="PWM[0-1]" library="Standard" deviceset="JST6" device="" package3d_urn="urn:adsk.eagle:package:12077888/1"/>
<part name="PWM[2-3]" library="Standard" deviceset="JST6" device="" package3d_urn="urn:adsk.eagle:package:12077888/1"/>
<part name="PWM[4-5]" library="Standard" deviceset="JST6" device="" package3d_urn="urn:adsk.eagle:package:12077888/1"/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="VCC/CAN" library="Standard" deviceset="JST6" device="" package3d_urn="urn:adsk.eagle:package:12077888/1"/>
<part name="UREG1" library="Standard" deviceset="UREG" device="" package3d_urn="urn:adsk.eagle:package:11372008/2" value="5V"/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C6" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="1µF"/>
<part name="C7" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="1µF"/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="162.56" y="22.86" size="1.778" layer="94" font="vector" align="top-left">HandController Platine, zuständig
für die Steuerung der Finger, muss 
sechs Servos Steuern</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="205.74" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="205.74" y="10.16" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="219.075" y="5.08" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="UREG2" gate="G$1" x="53.34" y="162.56" smashed="yes">
<attribute name="NAME" x="48.26" y="166.37" size="0.8128" layer="95" font="vector"/>
<attribute name="VALUE" x="58.42" y="157.48" size="0.8128" layer="96" font="vector"/>
</instance>
<instance part="GND1" gate="1" x="25.4" y="142.24" smashed="yes">
<attribute name="VALUE" x="22.86" y="139.7" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="38.1" y="154.94" smashed="yes">
<attribute name="NAME" x="40.005" y="153.035" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="36.195" y="153.035" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="68.58" y="154.94" smashed="yes">
<attribute name="NAME" x="70.485" y="153.035" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="66.675" y="153.035" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="GND2" gate="1" x="81.28" y="142.24" smashed="yes">
<attribute name="VALUE" x="78.74" y="139.7" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="68.58" y="142.24" smashed="yes">
<attribute name="VALUE" x="66.04" y="139.7" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="38.1" y="142.24" smashed="yes">
<attribute name="VALUE" x="35.56" y="139.7" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="55.88" y="142.24" smashed="yes">
<attribute name="VALUE" x="53.34" y="139.7" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="91.44" y="157.48" smashed="yes">
<attribute name="NAME" x="93.345" y="155.575" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="89.535" y="155.575" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="R1" gate="G$1" x="45.72" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="44.45" y="91.44" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="46.99" y="91.44" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="+3V1" gate="G$1" x="55.88" y="60.96" smashed="yes">
<attribute name="VALUE" x="58.42" y="58.42" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND6" gate="1" x="55.88" y="40.64" smashed="yes">
<attribute name="VALUE" x="53.34" y="38.1" size="1.778" layer="96"/>
</instance>
<instance part="+3V2" gate="G$1" x="45.72" y="104.14" smashed="yes">
<attribute name="VALUE" x="43.18" y="99.06" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="96.52" y="157.48" smashed="yes">
<attribute name="NAME" x="98.425" y="155.575" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="94.615" y="155.575" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C5" gate="G$1" x="111.76" y="157.48" smashed="yes">
<attribute name="NAME" x="113.665" y="155.575" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="109.855" y="155.575" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="P+1" gate="1" x="106.68" y="172.72" smashed="yes">
<attribute name="VALUE" x="104.14" y="167.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V3" gate="G$1" x="101.6" y="172.72" smashed="yes">
<attribute name="VALUE" x="99.06" y="167.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND7" gate="1" x="104.14" y="142.24" smashed="yes">
<attribute name="VALUE" x="101.6" y="139.7" size="1.778" layer="96"/>
</instance>
<instance part="GND8" gate="1" x="55.88" y="71.12" smashed="yes">
<attribute name="VALUE" x="53.34" y="68.58" size="1.778" layer="96"/>
</instance>
<instance part="MCU1" gate="PF" x="30.48" y="91.44" smashed="yes">
<attribute name="NAME" x="19.05" y="96.52" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="MCU1" gate="PA" x="30.48" y="60.96" smashed="yes">
<attribute name="NAME" x="24.13" y="78.74" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="MCU1" gate="PB" x="30.48" y="25.4" smashed="yes">
<attribute name="NAME" x="24.13" y="35.56" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="MCU1" gate="PWR" x="81.28" y="157.48" smashed="yes"/>
<instance part="IC1" gate="ATA6561" x="68.58" y="71.12" smashed="yes">
<attribute name="NAME" x="60.96" y="78.74" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC1" gate="PWR" x="104.14" y="157.48" smashed="yes">
<attribute name="NAME" x="101.6" y="157.48" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="SWD1" gate="G$1" x="63.5" y="50.8" smashed="yes" rot="MR0">
<attribute name="NAME" x="71.12" y="58.42" size="0.8128" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="PWM[0-1]" gate="G$1" x="149.86" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="152.4" y="58.42" size="0.8128" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="PWM[2-3]" gate="G$1" x="149.86" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="152.4" y="81.28" size="0.8128" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="PWM[4-5]" gate="G$1" x="149.86" y="111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="152.4" y="104.14" size="0.8128" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="P+2" gate="VCC" x="142.24" y="127" smashed="yes">
<attribute name="VALUE" x="139.7" y="121.92" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+3" gate="VCC" x="142.24" y="104.14" smashed="yes">
<attribute name="VALUE" x="139.7" y="99.06" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+4" gate="VCC" x="142.24" y="81.28" smashed="yes">
<attribute name="VALUE" x="139.7" y="76.2" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND9" gate="1" x="132.08" y="101.6" smashed="yes">
<attribute name="VALUE" x="129.54" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="1" x="132.08" y="78.74" smashed="yes">
<attribute name="VALUE" x="129.54" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="132.08" y="55.88" smashed="yes">
<attribute name="VALUE" x="129.54" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="VCC/CAN" gate="G$1" x="17.78" y="157.48" smashed="yes">
<attribute name="NAME" x="15.24" y="165.1" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="UREG1" gate="G$1" x="55.88" y="134.62" smashed="yes">
<attribute name="NAME" x="50.8" y="138.43" size="0.8128" layer="95" font="vector"/>
<attribute name="VALUE" x="60.96" y="129.54" size="0.8128" layer="96" font="vector"/>
</instance>
<instance part="GND12" gate="1" x="53.34" y="114.3" smashed="yes">
<attribute name="VALUE" x="50.8" y="111.76" size="1.778" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="58.42" y="114.3" smashed="yes">
<attribute name="VALUE" x="55.88" y="111.76" size="1.778" layer="96"/>
</instance>
<instance part="GND14" gate="1" x="38.1" y="114.3" smashed="yes">
<attribute name="VALUE" x="35.56" y="111.76" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="68.58" y="114.3" smashed="yes">
<attribute name="VALUE" x="66.04" y="111.76" size="1.778" layer="96"/>
</instance>
<instance part="C6" gate="G$1" x="38.1" y="127" smashed="yes">
<attribute name="NAME" x="40.64" y="125.095" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="35.56" y="125.095" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C7" gate="G$1" x="68.58" y="127" smashed="yes">
<attribute name="NAME" x="71.12" y="125.095" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="66.04" y="125.095" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="P+5" gate="1" x="73.66" y="139.7" smashed="yes">
<attribute name="VALUE" x="71.12" y="134.62" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+6" gate="VCC" x="33.02" y="167.64" smashed="yes">
<attribute name="VALUE" x="30.48" y="165.1" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
<bus name="CAN:CAN_H,CAN_L">
<segment>
<wire x1="81.28" y1="66.04" x2="81.28" y2="106.68" width="0.762" layer="92"/>
<wire x1="81.28" y1="106.68" x2="30.48" y2="106.68" width="0.762" layer="92"/>
<wire x1="30.48" y1="106.68" x2="30.48" y2="157.48" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="PWM[0..5]">
<segment>
<wire x1="48.26" y1="25.4" x2="48.26" y2="73.66" width="0.762" layer="92"/>
<wire x1="48.26" y1="25.4" x2="124.46" y2="25.4" width="0.762" layer="92"/>
<wire x1="124.46" y1="25.4" x2="124.46" y2="119.38" width="0.762" layer="92"/>
<label x="76.2" y="27.94" size="1.778" layer="95"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="22.86" y1="152.4" x2="25.4" y2="152.4" width="0.1524" layer="91"/>
<wire x1="25.4" y1="152.4" x2="25.4" y2="149.86" width="0.1524" layer="91"/>
<wire x1="25.4" y1="149.86" x2="22.86" y2="149.86" width="0.1524" layer="91"/>
<wire x1="25.4" y1="149.86" x2="25.4" y2="144.78" width="0.1524" layer="91"/>
<junction x="25.4" y="149.86"/>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="VCC/CAN" gate="G$1" pin="5"/>
<pinref part="VCC/CAN" gate="G$1" pin="6"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="C1" gate="G$1" pin="P$2"/>
<wire x1="38.1" y1="144.78" x2="38.1" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="UREG2" gate="G$1" pin="GND"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="55.88" y1="157.48" x2="55.88" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="P$2"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="68.58" y1="149.86" x2="68.58" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="81.28" y1="149.86" x2="81.28" y2="147.32" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="P$2"/>
<wire x1="81.28" y1="147.32" x2="81.28" y2="144.78" width="0.1524" layer="91"/>
<wire x1="91.44" y1="152.4" x2="91.44" y2="147.32" width="0.1524" layer="91"/>
<wire x1="91.44" y1="147.32" x2="81.28" y2="147.32" width="0.1524" layer="91"/>
<junction x="81.28" y="147.32"/>
<pinref part="MCU1" gate="PWR" pin="VSS"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="58.42" y1="48.26" x2="55.88" y2="48.26" width="0.1524" layer="91"/>
<wire x1="55.88" y1="48.26" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
<pinref part="SWD1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="P$2"/>
<wire x1="96.52" y1="152.4" x2="96.52" y2="147.32" width="0.1524" layer="91"/>
<wire x1="96.52" y1="147.32" x2="104.14" y2="147.32" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="104.14" y1="147.32" x2="104.14" y2="144.78" width="0.1524" layer="91"/>
<wire x1="104.14" y1="147.32" x2="104.14" y2="149.86" width="0.1524" layer="91"/>
<junction x="104.14" y="147.32"/>
<pinref part="C5" gate="G$1" pin="P$2"/>
<wire x1="104.14" y1="147.32" x2="111.76" y2="147.32" width="0.1524" layer="91"/>
<wire x1="111.76" y1="147.32" x2="111.76" y2="152.4" width="0.1524" layer="91"/>
<pinref part="IC1" gate="PWR" pin="VSS"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="55.88" y1="73.66" x2="55.88" y2="76.2" width="0.1524" layer="91"/>
<wire x1="55.88" y1="76.2" x2="58.42" y2="76.2" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ATA6561" pin="STBY"/>
</segment>
<segment>
<pinref part="PWM[4-5]" gate="G$1" pin="1"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="144.78" y1="106.68" x2="132.08" y2="106.68" width="0.1524" layer="91"/>
<wire x1="132.08" y1="106.68" x2="132.08" y2="104.14" width="0.1524" layer="91"/>
<pinref part="PWM[4-5]" gate="G$1" pin="4"/>
<wire x1="144.78" y1="114.3" x2="132.08" y2="114.3" width="0.1524" layer="91"/>
<wire x1="132.08" y1="114.3" x2="132.08" y2="106.68" width="0.1524" layer="91"/>
<junction x="132.08" y="106.68"/>
</segment>
<segment>
<pinref part="PWM[2-3]" gate="G$1" pin="1"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="144.78" y1="83.82" x2="132.08" y2="83.82" width="0.1524" layer="91"/>
<wire x1="132.08" y1="83.82" x2="132.08" y2="81.28" width="0.1524" layer="91"/>
<pinref part="PWM[2-3]" gate="G$1" pin="4"/>
<wire x1="144.78" y1="91.44" x2="132.08" y2="91.44" width="0.1524" layer="91"/>
<wire x1="132.08" y1="91.44" x2="132.08" y2="83.82" width="0.1524" layer="91"/>
<junction x="132.08" y="83.82"/>
</segment>
<segment>
<pinref part="PWM[0-1]" gate="G$1" pin="1"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="144.78" y1="60.96" x2="132.08" y2="60.96" width="0.1524" layer="91"/>
<wire x1="132.08" y1="60.96" x2="132.08" y2="58.42" width="0.1524" layer="91"/>
<pinref part="PWM[0-1]" gate="G$1" pin="4"/>
<wire x1="144.78" y1="68.58" x2="132.08" y2="68.58" width="0.1524" layer="91"/>
<wire x1="132.08" y1="68.58" x2="132.08" y2="60.96" width="0.1524" layer="91"/>
<junction x="132.08" y="60.96"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="P$2"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="38.1" y1="121.92" x2="38.1" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="UREG1" gate="G$1" pin="EN"/>
<wire x1="53.34" y1="116.84" x2="53.34" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="UREG1" gate="G$1" pin="GND"/>
<wire x1="58.42" y1="116.84" x2="58.42" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="C7" gate="G$1" pin="P$2"/>
<wire x1="68.58" y1="116.84" x2="68.58" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="58.42" y1="50.8" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="PA" pin="PA13"/>
<pinref part="SWD1" gate="G$1" pin="SWD"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="35.56" y1="48.26" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
<wire x1="53.34" y1="48.26" x2="53.34" y2="53.34" width="0.1524" layer="91"/>
<wire x1="53.34" y1="53.34" x2="58.42" y2="53.34" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="PA" pin="PA14"/>
<pinref part="SWD1" gate="G$1" pin="SWCLK"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="P$1"/>
<wire x1="35.56" y1="86.36" x2="45.72" y2="86.36" width="0.1524" layer="91"/>
<wire x1="45.72" y1="86.36" x2="45.72" y2="88.9" width="0.1524" layer="91"/>
<wire x1="58.42" y1="45.72" x2="45.72" y2="45.72" width="0.1524" layer="91"/>
<wire x1="45.72" y1="45.72" x2="45.72" y2="86.36" width="0.1524" layer="91"/>
<junction x="45.72" y="86.36"/>
<pinref part="MCU1" gate="PF" pin="!RST"/>
<pinref part="SWD1" gate="G$1" pin="!RST"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<pinref part="R1" gate="G$1" pin="P$2"/>
<wire x1="45.72" y1="101.6" x2="45.72" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="58.42" y1="55.88" x2="55.88" y2="55.88" width="0.1524" layer="91"/>
<wire x1="55.88" y1="55.88" x2="55.88" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SWD1" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="101.6" y1="170.18" x2="101.6" y2="167.64" width="0.1524" layer="91"/>
<wire x1="101.6" y1="167.64" x2="101.6" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="P$1"/>
<wire x1="68.58" y1="160.02" x2="68.58" y2="162.56" width="0.1524" layer="91"/>
<wire x1="68.58" y1="162.56" x2="68.58" y2="167.64" width="0.1524" layer="91"/>
<wire x1="68.58" y1="167.64" x2="76.2" y2="167.64" width="0.1524" layer="91"/>
<wire x1="76.2" y1="167.64" x2="81.28" y2="167.64" width="0.1524" layer="91"/>
<wire x1="81.28" y1="167.64" x2="86.36" y2="167.64" width="0.1524" layer="91"/>
<wire x1="86.36" y1="167.64" x2="86.36" y2="165.1" width="0.1524" layer="91"/>
<wire x1="81.28" y1="165.1" x2="81.28" y2="167.64" width="0.1524" layer="91"/>
<junction x="81.28" y="167.64"/>
<wire x1="76.2" y1="165.1" x2="76.2" y2="167.64" width="0.1524" layer="91"/>
<junction x="76.2" y="167.64"/>
<pinref part="C3" gate="G$1" pin="P$1"/>
<wire x1="91.44" y1="162.56" x2="91.44" y2="167.64" width="0.1524" layer="91"/>
<wire x1="91.44" y1="167.64" x2="86.36" y2="167.64" width="0.1524" layer="91"/>
<junction x="86.36" y="167.64"/>
<wire x1="101.6" y1="167.64" x2="96.52" y2="167.64" width="0.1524" layer="91"/>
<junction x="101.6" y="167.64"/>
<junction x="91.44" y="167.64"/>
<pinref part="C4" gate="G$1" pin="P$1"/>
<wire x1="96.52" y1="167.64" x2="91.44" y2="167.64" width="0.1524" layer="91"/>
<wire x1="96.52" y1="162.56" x2="96.52" y2="167.64" width="0.1524" layer="91"/>
<junction x="96.52" y="167.64"/>
<pinref part="MCU1" gate="PWR" pin="VDD"/>
<pinref part="MCU1" gate="PWR" pin="VDDIO2"/>
<pinref part="MCU1" gate="PWR" pin="VDDA"/>
<pinref part="IC1" gate="PWR" pin="VDD1"/>
<pinref part="UREG2" gate="G$1" pin="OUT"/>
<wire x1="60.96" y1="162.56" x2="68.58" y2="162.56" width="0.1524" layer="91"/>
<junction x="68.58" y="162.56"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="P$1"/>
<wire x1="111.76" y1="162.56" x2="111.76" y2="167.64" width="0.1524" layer="91"/>
<wire x1="111.76" y1="167.64" x2="106.68" y2="167.64" width="0.1524" layer="91"/>
<wire x1="106.68" y1="167.64" x2="106.68" y2="165.1" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="106.68" y1="167.64" x2="106.68" y2="170.18" width="0.1524" layer="91"/>
<junction x="106.68" y="167.64"/>
<pinref part="IC1" gate="PWR" pin="VDD"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="P$1"/>
<wire x1="68.58" y1="132.08" x2="68.58" y2="134.62" width="0.1524" layer="91"/>
<pinref part="UREG1" gate="G$1" pin="OUT"/>
<wire x1="68.58" y1="134.62" x2="63.5" y2="134.62" width="0.1524" layer="91"/>
<wire x1="68.58" y1="134.62" x2="73.66" y2="134.62" width="0.1524" layer="91"/>
<junction x="68.58" y="134.62"/>
<wire x1="73.66" y1="134.62" x2="73.66" y2="137.16" width="0.1524" layer="91"/>
<pinref part="P+5" gate="1" pin="+5V"/>
</segment>
</net>
<net name="CAN_H" class="0">
<segment>
<wire x1="22.86" y1="157.48" x2="30.48" y2="157.48" width="0.1524" layer="91"/>
<pinref part="VCC/CAN" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="78.74" y1="76.2" x2="81.28" y2="76.2" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ATA6561" pin="CANH"/>
</segment>
</net>
<net name="CAN_L" class="0">
<segment>
<wire x1="22.86" y1="154.94" x2="30.48" y2="154.94" width="0.1524" layer="91"/>
<pinref part="VCC/CAN" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="78.74" y1="66.04" x2="81.28" y2="66.04" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ATA6561" pin="CANL"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="58.42" y1="66.04" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
<wire x1="53.34" y1="66.04" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
<wire x1="53.34" y1="55.88" x2="35.56" y2="55.88" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="PA" pin="PA9"/>
<pinref part="IC1" gate="ATA6561" pin="RXD"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="35.56" y1="53.34" x2="50.8" y2="53.34" width="0.1524" layer="91"/>
<wire x1="50.8" y1="53.34" x2="50.8" y2="68.58" width="0.1524" layer="91"/>
<wire x1="50.8" y1="68.58" x2="58.42" y2="68.58" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="PA" pin="PA10"/>
<pinref part="IC1" gate="ATA6561" pin="TXD"/>
</segment>
</net>
<net name="PWM0" class="0">
<segment>
<pinref part="MCU1" gate="PA" pin="PA1"/>
<wire x1="35.56" y1="73.66" x2="48.26" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM[0-1]" gate="G$1" pin="3"/>
<wire x1="124.46" y1="66.04" x2="144.78" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM1" class="0">
<segment>
<pinref part="MCU1" gate="PA" pin="PA2"/>
<wire x1="35.56" y1="71.12" x2="48.26" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM[0-1]" gate="G$1" pin="6"/>
<wire x1="124.46" y1="73.66" x2="144.78" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<pinref part="MCU1" gate="PA" pin="PA3"/>
<wire x1="35.56" y1="68.58" x2="48.26" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM[2-3]" gate="G$1" pin="3"/>
<wire x1="124.46" y1="88.9" x2="144.78" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM3" class="0">
<segment>
<pinref part="MCU1" gate="PB" pin="PB0"/>
<wire x1="35.56" y1="33.02" x2="48.26" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM[2-3]" gate="G$1" pin="6"/>
<wire x1="124.46" y1="96.52" x2="144.78" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM4" class="0">
<segment>
<pinref part="MCU1" gate="PB" pin="PB1"/>
<wire x1="35.56" y1="30.48" x2="48.26" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM[4-5]" gate="G$1" pin="3"/>
<wire x1="124.46" y1="111.76" x2="144.78" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM5" class="0">
<segment>
<pinref part="MCU1" gate="PB" pin="PB4"/>
<wire x1="35.56" y1="25.4" x2="48.26" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM[4-5]" gate="G$1" pin="6"/>
<wire x1="124.46" y1="119.38" x2="144.78" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<wire x1="22.86" y1="160.02" x2="25.4" y2="160.02" width="0.1524" layer="91"/>
<wire x1="25.4" y1="160.02" x2="25.4" y2="162.56" width="0.1524" layer="91"/>
<wire x1="25.4" y1="162.56" x2="33.02" y2="162.56" width="0.1524" layer="91"/>
<wire x1="22.86" y1="162.56" x2="25.4" y2="162.56" width="0.1524" layer="91"/>
<junction x="25.4" y="162.56"/>
<pinref part="VCC/CAN" gate="G$1" pin="2"/>
<pinref part="VCC/CAN" gate="G$1" pin="1"/>
<wire x1="33.02" y1="162.56" x2="33.02" y2="134.62" width="0.1524" layer="91"/>
<pinref part="UREG1" gate="G$1" pin="IN"/>
<wire x1="33.02" y1="134.62" x2="38.1" y2="134.62" width="0.1524" layer="91"/>
<pinref part="UREG2" gate="G$1" pin="IN"/>
<wire x1="38.1" y1="134.62" x2="48.26" y2="134.62" width="0.1524" layer="91"/>
<wire x1="43.18" y1="162.56" x2="45.72" y2="162.56" width="0.1524" layer="91"/>
<pinref part="UREG2" gate="G$1" pin="EN"/>
<wire x1="50.8" y1="157.48" x2="50.8" y2="154.94" width="0.1524" layer="91"/>
<wire x1="50.8" y1="154.94" x2="43.18" y2="154.94" width="0.1524" layer="91"/>
<wire x1="43.18" y1="154.94" x2="43.18" y2="162.56" width="0.1524" layer="91"/>
<wire x1="33.02" y1="162.56" x2="38.1" y2="162.56" width="0.1524" layer="91"/>
<junction x="33.02" y="162.56"/>
<junction x="43.18" y="162.56"/>
<pinref part="C1" gate="G$1" pin="P$1"/>
<wire x1="38.1" y1="162.56" x2="43.18" y2="162.56" width="0.1524" layer="91"/>
<wire x1="38.1" y1="160.02" x2="38.1" y2="162.56" width="0.1524" layer="91"/>
<junction x="38.1" y="162.56"/>
<pinref part="C6" gate="G$1" pin="P$1"/>
<wire x1="38.1" y1="132.08" x2="38.1" y2="134.62" width="0.1524" layer="91"/>
<junction x="38.1" y="134.62"/>
<pinref part="P+6" gate="VCC" pin="VCC"/>
<wire x1="33.02" y1="162.56" x2="33.02" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM[0-1]" gate="G$1" pin="5"/>
<wire x1="142.24" y1="71.12" x2="144.78" y2="71.12" width="0.1524" layer="91"/>
<pinref part="PWM[0-1]" gate="G$1" pin="2"/>
<wire x1="142.24" y1="71.12" x2="142.24" y2="63.5" width="0.1524" layer="91"/>
<wire x1="142.24" y1="63.5" x2="144.78" y2="63.5" width="0.1524" layer="91"/>
<pinref part="P+4" gate="VCC" pin="VCC"/>
<wire x1="142.24" y1="71.12" x2="142.24" y2="78.74" width="0.1524" layer="91"/>
<junction x="142.24" y="71.12"/>
</segment>
<segment>
<pinref part="PWM[2-3]" gate="G$1" pin="5"/>
<wire x1="142.24" y1="93.98" x2="144.78" y2="93.98" width="0.1524" layer="91"/>
<pinref part="PWM[2-3]" gate="G$1" pin="2"/>
<wire x1="142.24" y1="93.98" x2="142.24" y2="86.36" width="0.1524" layer="91"/>
<wire x1="142.24" y1="86.36" x2="144.78" y2="86.36" width="0.1524" layer="91"/>
<pinref part="P+3" gate="VCC" pin="VCC"/>
<wire x1="142.24" y1="93.98" x2="142.24" y2="101.6" width="0.1524" layer="91"/>
<junction x="142.24" y="93.98"/>
</segment>
<segment>
<pinref part="PWM[4-5]" gate="G$1" pin="5"/>
<wire x1="142.24" y1="116.84" x2="144.78" y2="116.84" width="0.1524" layer="91"/>
<pinref part="PWM[4-5]" gate="G$1" pin="2"/>
<wire x1="142.24" y1="116.84" x2="142.24" y2="109.22" width="0.1524" layer="91"/>
<wire x1="142.24" y1="109.22" x2="144.78" y2="109.22" width="0.1524" layer="91"/>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<wire x1="142.24" y1="116.84" x2="142.24" y2="124.46" width="0.1524" layer="91"/>
<junction x="142.24" y="116.84"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
