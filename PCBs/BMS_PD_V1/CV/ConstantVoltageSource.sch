<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.4.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Standard">
<packages>
<package name="RESC1005X30" urn="urn:adsk.eagle:footprint:11214493/1" locally_modified="yes">
<description>Chip, 1.00 X 0.50 X 0.30 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.30 mm&lt;/p&gt;</description>
<smd name="1" x="-0.5" y="0" dx="0.5118" dy="0.6118" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.5118" dy="0.6118" layer="1"/>
<text x="0" y="0.4929" size="0.6096" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.4929" size="0.6096" layer="27" align="top-center">&gt;VALUE</text>
<wire x1="-0.127" y1="0.254" x2="-0.127" y2="-0.254" width="0.0762" layer="21"/>
<wire x1="-0.127" y1="-0.254" x2="0.127" y2="-0.254" width="0.0762" layer="21"/>
<wire x1="0.127" y1="-0.254" x2="0.127" y2="0.254" width="0.0762" layer="21"/>
<wire x1="0.127" y1="0.254" x2="-0.127" y2="0.254" width="0.0762" layer="21"/>
</package>
<package name="CAPC1005X30" urn="urn:adsk.eagle:footprint:11214500/1" locally_modified="yes">
<description>Chip, 1.00 X 0.50 X 0.30 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.30 mm&lt;/p&gt;</description>
<smd name="1" x="-0.5" y="0" dx="0.5118" dy="0.6118" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.5118" dy="0.6118" layer="1"/>
<text x="0" y="0.4929" size="0.6096" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.4929" size="0.6096" layer="27" align="top-center">&gt;VALUE</text>
<polygon width="0.0762" layer="21">
<vertex x="-0.127" y="0.254"/>
<vertex x="-0.127" y="-0.254"/>
<vertex x="0.127" y="-0.254"/>
<vertex x="0.127" y="0.254"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="RESC1005X30" urn="urn:adsk.eagle:package:11214492/1" locally_modified="yes" type="model">
<description>Chip, 1.00 X 0.50 X 0.30 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.30 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1005X30"/>
</packageinstances>
</package3d>
<package3d name="CAPC1005X30" urn="urn:adsk.eagle:package:11214499/1" locally_modified="yes" type="model">
<description>Chip, 1.00 X 0.50 X 0.30 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.30 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1005X30"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="203.835" y2="24.13" width="0.1016" layer="94"/>
<wire x1="203.835" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="203.835" y2="8.89" width="0.1016" layer="94"/>
<wire x1="203.835" y1="8.89" x2="203.835" y2="3.81" width="0.1016" layer="94"/>
<wire x1="203.835" y1="8.89" x2="203.835" y2="13.97" width="0.1016" layer="94"/>
<wire x1="203.835" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="203.835" y1="13.97" x2="203.835" y2="19.05" width="0.1016" layer="94"/>
<wire x1="203.835" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="203.835" y1="19.05" x2="203.835" y2="24.13" width="0.1016" layer="94"/>
<text x="205.74" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="205.74" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="219.075" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="205.486" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
<text x="205.74" y="20.32" size="2.54" layer="94" font="vector">Moritz Geier</text>
</symbol>
<symbol name="R">
<wire x1="-2.54" y1="-0.9525" x2="-2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.9525" x2="2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.9525" x2="-2.54" y2="-0.9525" width="0.1524" layer="94"/>
<pin name="P$1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="-2.54" y="1.27" size="0.8128" layer="95">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="0.8128" layer="95" align="top-left">&gt;VALUE</text>
</symbol>
<symbol name="C">
<pin name="P$1" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="P$2" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="-1.905" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="1.905" y2="0.635" width="0.3048" layer="94"/>
<text x="2.54" y="-1.905" size="0.8128" layer="95" rot="R90" align="top-left">&gt;NAME</text>
<text x="-2.54" y="-1.905" size="0.8128" layer="95" rot="R90">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="RESC1005X30">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11214492/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X30">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11214499/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BMS_PD">
<packages>
<package name="QFN50P400X400X80-21T275" urn="urn:adsk.eagle:footprint:11816613/1" locally_modified="yes">
<description>20-QFN, 0.50 mm pitch, 4.00 X 4.00 X 0.80 mm body, 2.75 X 2.75 mm thermal pad
&lt;p&gt;20-pin QFN package with 0.50 mm pitch with body size 4.00 X 4.00 X 0.80 mm and thermal pad size 2.75 X 2.75 mm&lt;/p&gt;</description>
<circle x="-2.554" y="1.639" radius="0.25" width="0" layer="21"/>
<wire x1="-2.05" y1="1.389" x2="-2.05" y2="2.05" width="0.12" layer="21"/>
<wire x1="-2.05" y1="2.05" x2="-1.389" y2="2.05" width="0.12" layer="21"/>
<wire x1="2.05" y1="1.389" x2="2.05" y2="2.05" width="0.12" layer="21"/>
<wire x1="2.05" y1="2.05" x2="1.389" y2="2.05" width="0.12" layer="21"/>
<wire x1="2.05" y1="-1.389" x2="2.05" y2="-2.05" width="0.12" layer="21"/>
<wire x1="2.05" y1="-2.05" x2="1.389" y2="-2.05" width="0.12" layer="21"/>
<wire x1="-2.05" y1="-1.389" x2="-2.05" y2="-2.05" width="0.12" layer="21"/>
<wire x1="-2.05" y1="-2.05" x2="-1.389" y2="-2.05" width="0.12" layer="21"/>
<smd name="1" x="-1.9346" y="1" dx="0.86" dy="0.27" layer="1" roundness="100"/>
<smd name="2" x="-1.9346" y="0.5" dx="0.86" dy="0.27" layer="1" roundness="100"/>
<smd name="3" x="-1.9346" y="0" dx="0.86" dy="0.27" layer="1" roundness="100"/>
<smd name="4" x="-1.9346" y="-0.5" dx="0.86" dy="0.27" layer="1" roundness="100"/>
<smd name="5" x="-1.9346" y="-1" dx="0.86" dy="0.27" layer="1" roundness="100"/>
<smd name="6" x="-1" y="-1.9346" dx="0.86" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="-0.5" y="-1.9346" dx="0.86" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="8" x="0" y="-1.9346" dx="0.86" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="0.5" y="-1.9346" dx="0.86" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="1" y="-1.9346" dx="0.86" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="1.9346" y="-1" dx="0.86" dy="0.27" layer="1" roundness="100"/>
<smd name="12" x="1.9346" y="-0.5" dx="0.86" dy="0.27" layer="1" roundness="100"/>
<smd name="13" x="1.9346" y="0" dx="0.86" dy="0.27" layer="1" roundness="100"/>
<smd name="14" x="1.9346" y="0.5" dx="0.86" dy="0.27" layer="1" roundness="100"/>
<smd name="15" x="1.9346" y="1" dx="0.86" dy="0.27" layer="1" roundness="100"/>
<smd name="16" x="1" y="1.9346" dx="0.86" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="0.5" y="1.9346" dx="0.86" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="18" x="0" y="1.9346" dx="0.86" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="19" x="-0.5" y="1.9346" dx="0.86" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="20" x="-1" y="1.9346" dx="0.86" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="21" x="0" y="0" dx="2.75" dy="2.75" layer="1" thermals="no"/>
<text x="0" y="2.9996" size="0.6096" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.9996" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3216X160" urn="urn:adsk.eagle:footprint:11819575/1" locally_modified="yes">
<description>Chip, 3.20 X 1.60 X 1.60 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 1.60 mm&lt;/p&gt;</description>
<smd name="1" x="-1.4" y="0" dx="1.2118" dy="1.7118" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.2118" dy="1.7118" layer="1"/>
<text x="0" y="1.1699" size="0.6096" layer="25" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1699" size="0.6096" layer="27" font="vector" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.635" y1="-0.635" x2="0.635" y2="0.635" layer="21"/>
</package>
<package name="L_140120">
<smd name="P$1" x="-6.5" y="0" dx="3.3" dy="4.9" layer="1"/>
<smd name="P$2" x="6.5" y="0" dx="3.3" dy="4.9" layer="1"/>
<wire x1="-7" y1="6" x2="-7" y2="3" width="0.1524" layer="21"/>
<wire x1="-7" y1="6" x2="7" y2="6" width="0.1524" layer="21"/>
<wire x1="7" y1="6" x2="7" y2="3" width="0.1524" layer="21"/>
<wire x1="7" y1="-3" x2="7" y2="-7" width="0.1524" layer="21"/>
<wire x1="7" y1="-7" x2="-7" y2="-7" width="0.1524" layer="21"/>
<wire x1="-7" y1="-7" x2="-7" y2="-3" width="0.1524" layer="21"/>
<text x="-5.715" y="4.445" size="0.8128" layer="25" font="vector">&gt;NAME</text>
<text x="-6.35" y="-6.35" size="0.8128" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="L_100115">
<smd name="P$1" x="-4.75" y="0" dx="4" dy="3.5" layer="1" rot="R90"/>
<smd name="P$2" x="4.75" y="0" dx="4" dy="3.5" layer="1" rot="R90"/>
<wire x1="-6" y1="2.25" x2="-6" y2="5.25" width="0.127" layer="21"/>
<wire x1="-6" y1="5.25" x2="6" y2="5.25" width="0.127" layer="21"/>
<wire x1="6" y1="5.25" x2="6" y2="2.25" width="0.127" layer="21"/>
<wire x1="6" y1="-2.25" x2="6" y2="-5.25" width="0.127" layer="21"/>
<wire x1="6" y1="-5.25" x2="-6" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-6" y1="-5.25" x2="-6" y2="-2.25" width="0.127" layer="21"/>
<text x="-5.25" y="4.25" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-5.5" y="-4.75" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="PG-TDSON-8">
<smd name="3" x="2.825" y="0.635" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="2" x="2.825" y="-0.635" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="1" x="2.825" y="-1.905" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="4" x="2.825" y="1.905" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="6" x="-2.825" y="0.635" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="7" x="-2.825" y="-0.635" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="8" x="-2.825" y="-1.905" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="5" x="-2.825" y="1.905" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<polygon width="0.002540625" layer="1">
<vertex x="-3.475" y="-2.28"/>
<vertex x="-3.475" y="-1.53"/>
<vertex x="-2.175" y="-1.53"/>
<vertex x="-2.175" y="2.285"/>
<vertex x="1.625" y="2.28"/>
<vertex x="1.625" y="-2.28"/>
</polygon>
<circle x="3.81" y="-2.8575" radius="0.3175" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="2.54" x2="2.8575" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.8575" y1="-2.54" x2="-3.4925" y2="-2.54" width="0.127" layer="21"/>
<polygon width="0.127" layer="29">
<vertex x="-3.475" y="-2.28"/>
<vertex x="-3.475" y="-1.53"/>
<vertex x="-2.175" y="-1.53"/>
<vertex x="-2.175" y="-1.01"/>
<vertex x="-3.475" y="-1.01"/>
<vertex x="-2.175" y="-1.01"/>
<vertex x="-2.175" y="-0.26"/>
<vertex x="-2.175" y="0.26"/>
<vertex x="-3.475" y="0.26"/>
<vertex x="-3.475" y="1.01"/>
<vertex x="-2.175" y="1.01"/>
<vertex x="-2.175" y="1.53"/>
<vertex x="-3.475" y="1.53"/>
<vertex x="-3.475" y="2.28"/>
<vertex x="1.625" y="2.28"/>
<vertex x="1.625" y="-2.28"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.475" y="-2.28"/>
<vertex x="-3.475" y="-1.53"/>
<vertex x="-2.175" y="-1.53"/>
<vertex x="-2.175" y="-1.01"/>
<vertex x="-3.475" y="-1.01"/>
<vertex x="-2.175" y="-1.01"/>
<vertex x="-2.175" y="-0.26"/>
<vertex x="-2.175" y="0.26"/>
<vertex x="-3.475" y="0.26"/>
<vertex x="-3.475" y="1.01"/>
<vertex x="-2.175" y="1.01"/>
<vertex x="-2.175" y="1.53"/>
<vertex x="-3.475" y="1.53"/>
<vertex x="-3.475" y="2.28"/>
<vertex x="1.625" y="2.28"/>
<vertex x="1.625" y="-2.28"/>
</polygon>
<text x="-3.4925" y="2.8575" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-3.4925" y="-3.4925" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="QFN50P400X400X80-21T275" urn="urn:adsk.eagle:package:11816611/1" type="model">
<description>20-QFN, 0.50 mm pitch, 4.00 X 4.00 X 0.80 mm body, 2.75 X 2.75 mm thermal pad
&lt;p&gt;20-pin QFN package with 0.50 mm pitch with body size 4.00 X 4.00 X 0.80 mm and thermal pad size 2.75 X 2.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="QFN50P400X400X80-21T275"/>
</packageinstances>
</package3d>
<package3d name="CAPC3216X160" urn="urn:adsk.eagle:package:11819311/1" type="model">
<description>Chip, 3.20 X 1.60 X 1.60 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 1.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3216X160"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="ADP1853">
<pin name="ENB" x="-12.7" y="15.24" visible="pin" length="short"/>
<pin name="TRK" x="-12.7" y="12.7" visible="pin" length="short"/>
<pin name="VCCO" x="-12.7" y="10.16" visible="pin" length="short"/>
<pin name="SYNC" x="-12.7" y="2.54" visible="pin" length="short"/>
<pin name="FREQ" x="-12.7" y="-2.54" visible="pin" length="short"/>
<pin name="SS" x="-12.7" y="-5.08" visible="pin" length="short"/>
<pin name="RAMP" x="-12.7" y="17.78" visible="pin" length="short"/>
<pin name="DH" x="12.7" y="17.78" visible="pin" length="short" rot="R180"/>
<pin name="BST" x="12.7" y="15.24" visible="pin" length="short" rot="R180"/>
<pin name="SW" x="12.7" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="CS" x="12.7" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="ILIM" x="12.7" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="DL" x="12.7" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="FB" x="12.7" y="-10.16" visible="pin" length="short" rot="R180"/>
<pin name="COMP" x="12.7" y="-12.7" visible="pin" length="short" rot="R180"/>
<pin name="PGOOD" x="12.7" y="-17.78" visible="pin" length="short" rot="R180"/>
<pin name="CLKOUT" x="12.7" y="-20.32" visible="pin" length="short" rot="R180"/>
<wire x1="-10.16" y1="19.05" x2="-10.16" y2="-21.59" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-21.59" x2="10.16" y2="-21.59" width="0.254" layer="94"/>
<wire x1="10.16" y1="-21.59" x2="10.16" y2="19.05" width="0.254" layer="94"/>
<wire x1="10.16" y1="19.05" x2="-10.16" y2="19.05" width="0.254" layer="94"/>
<text x="-10.16" y="20.32" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="PWR">
<pin name="VDD" x="0" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="VSS" x="0" y="-7.62" visible="off" length="short" direction="pwr" rot="R90"/>
<circle x="0" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="4.064" width="0.1524" layer="94"/>
<circle x="0" y="-3.048" radius="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-4.064" width="0.1524" layer="94"/>
<text x="-0.635" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VDD</text>
<text x="-0.635" y="-5.08" size="0.8128" layer="95" font="vector" rot="R90" align="bottom-right">VSS</text>
<text x="-2.54" y="0" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="C">
<pin name="P$1" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="P$2" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="-1.905" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="1.905" y2="0.635" width="0.3048" layer="94"/>
<text x="2.54" y="-1.905" size="0.8128" layer="95" rot="R90" align="top-left">&gt;NAME</text>
<text x="-2.54" y="-1.905" size="0.8128" layer="95" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="L">
<wire x1="-2.54" y1="-0.9525" x2="-2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.9525" x2="2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.9525" x2="-2.54" y2="-0.9525" width="0.1524" layer="94"/>
<pin name="P$1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="-2.54" y="1.27" size="0.8128" layer="95">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="0.8128" layer="95" align="top-left">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-0.9525" x2="2.54" y2="0.9525" layer="94"/>
</symbol>
<symbol name="NMOS">
<description>MOSFET N-channel - Enhancement mode</description>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.254" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.254" width="0" layer="94"/>
<text x="6.35" y="-1.27" size="0.8128" layer="96" rot="MR270" align="bottom-right">&gt;VALUE</text>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.508"/>
<vertex x="2.032" y="-0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
<text x="-1.27" y="-1.27" size="0.8128" layer="95" font="vector" rot="R90">&gt;NAME</text>
</symbol>
<symbol name="VBAT">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VBAT" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="PGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADP1853" prefix="IC">
<gates>
<gate name="ADP1853" symbol="ADP1853" x="0" y="0"/>
<gate name="PWR" symbol="PWR" x="-30.48" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="QFN50P400X400X80-21T275">
<connects>
<connect gate="ADP1853" pin="BST" pad="15"/>
<connect gate="ADP1853" pin="CLKOUT" pad="7"/>
<connect gate="ADP1853" pin="COMP" pad="4"/>
<connect gate="ADP1853" pin="CS" pad="12"/>
<connect gate="ADP1853" pin="DH" pad="14"/>
<connect gate="ADP1853" pin="DL" pad="11"/>
<connect gate="ADP1853" pin="ENB" pad="1"/>
<connect gate="ADP1853" pin="FB" pad="3"/>
<connect gate="ADP1853" pin="FREQ" pad="19"/>
<connect gate="ADP1853" pin="ILIM" pad="16"/>
<connect gate="ADP1853" pin="PGOOD" pad="17"/>
<connect gate="ADP1853" pin="RAMP" pad="18"/>
<connect gate="ADP1853" pin="SS" pad="2"/>
<connect gate="ADP1853" pin="SW" pad="13"/>
<connect gate="ADP1853" pin="SYNC" pad="6"/>
<connect gate="ADP1853" pin="TRK" pad="20"/>
<connect gate="ADP1853" pin="VCCO" pad="9"/>
<connect gate="PWR" pin="VDD" pad="8"/>
<connect gate="PWR" pin="VSS" pad="5 10 21"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11816611/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC3216X160">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11819311/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="140120" package="L_140120">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="100115" package="L_100115">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NMOS" prefix="M" uservalue="yes">
<gates>
<gate name="G$1" symbol="NMOS" x="-2.54" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="PG-TDSON-8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VBAT" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VBAT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PGND" prefix="PGND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="PGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="Standard" deviceset="A4L-LOC" device=""/>
<part name="IC1" library="BMS_PD" deviceset="ADP1853" device="" package3d_urn="urn:adsk.eagle:package:11816611/1"/>
<part name="R1" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/1"/>
<part name="R2" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/1" value="10k"/>
<part name="R3" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/1" value="22k"/>
<part name="R4" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/1" value="1k"/>
<part name="R5" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/1" value="13k"/>
<part name="C1" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="1µF"/>
<part name="C2" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="10nF"/>
<part name="C3" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="1µF"/>
<part name="C4" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="100nF"/>
<part name="C5" library="BMS_PD" deviceset="C" device="" package3d_urn="urn:adsk.eagle:package:11819311/1" value="100µF"/>
<part name="C6" library="BMS_PD" deviceset="C" device="" package3d_urn="urn:adsk.eagle:package:11819311/1" value="100µF"/>
<part name="L1" library="BMS_PD" deviceset="L" device="100115" value="1µH"/>
<part name="M1" library="BMS_PD" deviceset="NMOS" device="" value="BSC010NE2LS"/>
<part name="M2" library="BMS_PD" deviceset="NMOS" device="" value="BSC010NE2LS"/>
<part name="P+1" library="BMS_PD" deviceset="VBAT" device=""/>
<part name="PGND1" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND2" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND3" library="BMS_PD" deviceset="PGND" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="IC2" library="BMS_PD" deviceset="ADP1853" device="" package3d_urn="urn:adsk.eagle:package:11816611/1"/>
<part name="R6" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/1"/>
<part name="R7" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/1" value="10k"/>
<part name="R8" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/1" value="22k"/>
<part name="C7" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="1µF"/>
<part name="C9" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="1µF"/>
<part name="C10" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="100nF"/>
<part name="C11" library="BMS_PD" deviceset="C" device="" package3d_urn="urn:adsk.eagle:package:11819311/1" value="100µF"/>
<part name="C12" library="BMS_PD" deviceset="C" device="" package3d_urn="urn:adsk.eagle:package:11819311/1" value="100µF"/>
<part name="L2" library="BMS_PD" deviceset="L" device="100115" value="1µH"/>
<part name="M3" library="BMS_PD" deviceset="NMOS" device="" value="BSC010NE2LS"/>
<part name="M4" library="BMS_PD" deviceset="NMOS" device="" value="BSC010NE2LS"/>
<part name="P+4" library="BMS_PD" deviceset="VBAT" device=""/>
<part name="PGND4" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND5" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND6" library="BMS_PD" deviceset="PGND" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C8" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="1µF"/>
<part name="C13" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/1" value="1µF"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="205.74" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="205.74" y="10.16" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="219.075" y="5.08" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="IC1" gate="ADP1853" x="43.18" y="86.36" smashed="yes">
<attribute name="NAME" x="33.02" y="106.68" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="R1" gate="G$1" x="22.86" y="104.14" smashed="yes">
<attribute name="NAME" x="20.32" y="105.41" size="0.8128" layer="95"/>
<attribute name="VALUE" x="20.32" y="102.87" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R2" gate="G$1" x="66.04" y="91.44" smashed="yes">
<attribute name="NAME" x="63.5" y="92.71" size="0.8128" layer="95"/>
<attribute name="VALUE" x="63.5" y="90.17" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R3" gate="G$1" x="66.04" y="78.74" smashed="yes">
<attribute name="NAME" x="63.5" y="80.01" size="0.8128" layer="95"/>
<attribute name="VALUE" x="63.5" y="77.47" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R4" gate="G$1" x="101.6" y="68.58" smashed="yes" rot="R270">
<attribute name="NAME" x="102.87" y="71.12" size="0.8128" layer="95" rot="R270"/>
<attribute name="VALUE" x="100.33" y="71.12" size="0.8128" layer="95" rot="R270" align="top-left"/>
</instance>
<instance part="R5" gate="G$1" x="101.6" y="83.82" smashed="yes" rot="R270">
<attribute name="NAME" x="102.87" y="86.36" size="0.8128" layer="95" rot="R270"/>
<attribute name="VALUE" x="100.33" y="86.36" size="0.8128" layer="95" rot="R270" align="top-left"/>
</instance>
<instance part="C1" gate="G$1" x="15.24" y="73.66" smashed="yes">
<attribute name="NAME" x="17.78" y="71.755" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="12.7" y="71.755" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="22.86" y="73.66" smashed="yes">
<attribute name="NAME" x="25.4" y="71.755" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="20.32" y="71.755" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C3" gate="G$1" x="86.36" y="106.68" smashed="yes">
<attribute name="NAME" x="88.9" y="104.775" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="83.82" y="104.775" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="66.04" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="67.945" y="104.14" size="0.8128" layer="95" rot="R180" align="top-left"/>
<attribute name="VALUE" x="67.945" y="99.06" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="C5" gate="G$1" x="109.22" y="83.82" smashed="yes" rot="R180">
<attribute name="NAME" x="106.68" y="85.725" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="111.76" y="85.725" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C6" gate="G$1" x="116.84" y="83.82" smashed="yes" rot="R180">
<attribute name="NAME" x="114.3" y="85.725" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="119.38" y="85.725" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="L1" gate="G$1" x="93.98" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="96.52" y="92.71" size="0.8128" layer="95" rot="R180"/>
<attribute name="VALUE" x="96.52" y="95.25" size="0.8128" layer="95" rot="R180" align="top-left"/>
</instance>
<instance part="M1" gate="G$1" x="76.2" y="86.36" smashed="yes">
<attribute name="VALUE" x="82.55" y="85.09" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="74.93" y="85.09" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="M2" gate="G$1" x="76.2" y="106.68" smashed="yes">
<attribute name="VALUE" x="82.55" y="105.41" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="74.93" y="105.41" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="P+1" gate="VCC" x="116.84" y="99.06" smashed="yes">
<attribute name="VALUE" x="114.3" y="96.52" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PGND1" gate="1" x="78.74" y="55.88" smashed="yes">
<attribute name="VALUE" x="76.2" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="PGND2" gate="1" x="109.22" y="55.88" smashed="yes">
<attribute name="VALUE" x="106.68" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="PGND3" gate="1" x="116.84" y="55.88" smashed="yes">
<attribute name="VALUE" x="114.3" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="22.86" y="55.88" smashed="yes">
<attribute name="VALUE" x="20.32" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="GND2" gate="1" x="15.24" y="55.88" smashed="yes">
<attribute name="VALUE" x="12.7" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="101.6" y="55.88" smashed="yes">
<attribute name="VALUE" x="99.06" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="86.36" y="55.88" smashed="yes">
<attribute name="VALUE" x="83.82" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="P+2" gate="VCC" x="78.74" y="119.38" smashed="yes">
<attribute name="VALUE" x="76.2" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+3" gate="VCC" x="15.24" y="119.38" smashed="yes">
<attribute name="VALUE" x="12.7" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="IC2" gate="ADP1853" x="165.1" y="86.36" smashed="yes">
<attribute name="NAME" x="154.94" y="106.68" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="R6" gate="G$1" x="144.78" y="104.14" smashed="yes">
<attribute name="NAME" x="142.24" y="105.41" size="0.8128" layer="95"/>
<attribute name="VALUE" x="142.24" y="102.87" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R7" gate="G$1" x="187.96" y="91.44" smashed="yes">
<attribute name="NAME" x="185.42" y="92.71" size="0.8128" layer="95"/>
<attribute name="VALUE" x="185.42" y="90.17" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R8" gate="G$1" x="187.96" y="78.74" smashed="yes">
<attribute name="NAME" x="185.42" y="80.01" size="0.8128" layer="95"/>
<attribute name="VALUE" x="185.42" y="77.47" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="C7" gate="G$1" x="137.16" y="73.66" smashed="yes">
<attribute name="NAME" x="139.7" y="71.755" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="134.62" y="71.755" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C9" gate="G$1" x="208.28" y="106.68" smashed="yes">
<attribute name="NAME" x="210.82" y="104.775" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="205.74" y="104.775" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C10" gate="G$1" x="187.96" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="189.865" y="104.14" size="0.8128" layer="95" rot="R180" align="top-left"/>
<attribute name="VALUE" x="189.865" y="99.06" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="C11" gate="G$1" x="231.14" y="83.82" smashed="yes" rot="R180">
<attribute name="NAME" x="228.6" y="85.725" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="233.68" y="85.725" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C12" gate="G$1" x="238.76" y="83.82" smashed="yes" rot="R180">
<attribute name="NAME" x="236.22" y="85.725" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="241.3" y="85.725" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="L2" gate="G$1" x="215.9" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="218.44" y="92.71" size="0.8128" layer="95" rot="R180"/>
<attribute name="VALUE" x="218.44" y="95.25" size="0.8128" layer="95" rot="R180" align="top-left"/>
</instance>
<instance part="M3" gate="G$1" x="198.12" y="86.36" smashed="yes">
<attribute name="VALUE" x="204.47" y="85.09" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="196.85" y="85.09" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="M4" gate="G$1" x="198.12" y="106.68" smashed="yes">
<attribute name="VALUE" x="204.47" y="105.41" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="196.85" y="105.41" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="P+4" gate="VCC" x="238.76" y="99.06" smashed="yes">
<attribute name="VALUE" x="236.22" y="96.52" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PGND4" gate="1" x="200.66" y="55.88" smashed="yes">
<attribute name="VALUE" x="198.12" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="PGND5" gate="1" x="231.14" y="55.88" smashed="yes">
<attribute name="VALUE" x="228.6" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="PGND6" gate="1" x="238.76" y="55.88" smashed="yes">
<attribute name="VALUE" x="236.22" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="GND6" gate="1" x="137.16" y="55.88" smashed="yes">
<attribute name="VALUE" x="134.62" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="GND8" gate="1" x="208.28" y="55.88" smashed="yes">
<attribute name="VALUE" x="205.74" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="P+5" gate="VCC" x="200.66" y="119.38" smashed="yes">
<attribute name="VALUE" x="198.12" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+6" gate="VCC" x="137.16" y="119.38" smashed="yes">
<attribute name="VALUE" x="134.62" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="IC1" gate="PWR" x="48.26" y="152.4" smashed="yes">
<attribute name="NAME" x="45.72" y="152.4" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC2" gate="PWR" x="73.66" y="152.4" smashed="yes">
<attribute name="NAME" x="71.12" y="152.4" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="P+7" gate="VCC" x="48.26" y="167.64" smashed="yes">
<attribute name="VALUE" x="45.72" y="165.1" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+8" gate="VCC" x="73.66" y="167.64" smashed="yes">
<attribute name="VALUE" x="71.12" y="165.1" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C8" gate="G$1" x="55.88" y="152.4" smashed="yes">
<attribute name="NAME" x="58.42" y="150.495" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="53.34" y="150.495" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C13" gate="G$1" x="81.28" y="152.4" smashed="yes">
<attribute name="NAME" x="83.82" y="150.495" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="78.74" y="150.495" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="GND5" gate="1" x="48.26" y="137.16" smashed="yes">
<attribute name="VALUE" x="45.72" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="73.66" y="137.16" smashed="yes">
<attribute name="VALUE" x="71.12" y="134.62" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
<bus name="SYNC:CLK,COMP,PGOOD,SS">
<segment>
<wire x1="27.94" y1="43.18" x2="182.88" y2="43.18" width="0.762" layer="92"/>
<label x="96.52" y="38.1" size="1.778" layer="95"/>
</segment>
</bus>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="IC1" gate="ADP1853" pin="RAMP"/>
<pinref part="R1" gate="G$1" pin="P$2"/>
<wire x1="30.48" y1="104.14" x2="27.94" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC1" gate="ADP1853" pin="TRK"/>
<wire x1="30.48" y1="99.06" x2="15.24" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="P$1"/>
<wire x1="15.24" y1="99.06" x2="15.24" y2="96.52" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ADP1853" pin="VCCO"/>
<wire x1="15.24" y1="96.52" x2="15.24" y2="78.74" width="0.1524" layer="91"/>
<wire x1="30.48" y1="96.52" x2="22.86" y2="96.52" width="0.1524" layer="91"/>
<junction x="15.24" y="96.52"/>
<pinref part="IC1" gate="ADP1853" pin="SYNC"/>
<wire x1="22.86" y1="96.52" x2="15.24" y2="96.52" width="0.1524" layer="91"/>
<wire x1="30.48" y1="88.9" x2="22.86" y2="88.9" width="0.1524" layer="91"/>
<wire x1="22.86" y1="88.9" x2="22.86" y2="96.52" width="0.1524" layer="91"/>
<junction x="22.86" y="96.52"/>
<pinref part="IC1" gate="ADP1853" pin="FREQ"/>
<wire x1="30.48" y1="83.82" x2="22.86" y2="83.82" width="0.1524" layer="91"/>
<wire x1="22.86" y1="83.82" x2="22.86" y2="88.9" width="0.1524" layer="91"/>
<junction x="22.86" y="88.9"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="P$2"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="22.86" y1="68.58" x2="22.86" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C1" gate="G$1" pin="P$2"/>
<wire x1="15.24" y1="58.42" x2="15.24" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="R4" gate="G$1" pin="P$2"/>
<wire x1="101.6" y1="58.42" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="C3" gate="G$1" pin="P$2"/>
<wire x1="86.36" y1="58.42" x2="86.36" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="C7" gate="G$1" pin="P$2"/>
<wire x1="137.16" y1="58.42" x2="137.16" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="C9" gate="G$1" pin="P$2"/>
<wire x1="208.28" y1="58.42" x2="208.28" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="IC1" gate="PWR" pin="VSS"/>
<wire x1="48.26" y1="139.7" x2="48.26" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="P$2"/>
<wire x1="48.26" y1="142.24" x2="48.26" y2="144.78" width="0.1524" layer="91"/>
<wire x1="55.88" y1="147.32" x2="55.88" y2="142.24" width="0.1524" layer="91"/>
<wire x1="55.88" y1="142.24" x2="48.26" y2="142.24" width="0.1524" layer="91"/>
<junction x="48.26" y="142.24"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="P$2"/>
<wire x1="81.28" y1="147.32" x2="81.28" y2="142.24" width="0.1524" layer="91"/>
<wire x1="81.28" y1="142.24" x2="73.66" y2="142.24" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="73.66" y1="142.24" x2="73.66" y2="139.7" width="0.1524" layer="91"/>
<pinref part="IC2" gate="PWR" pin="VSS"/>
<wire x1="73.66" y1="144.78" x2="73.66" y2="142.24" width="0.1524" layer="91"/>
<junction x="73.66" y="142.24"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC1" gate="ADP1853" pin="DL"/>
<pinref part="M1" gate="G$1" pin="G"/>
<wire x1="55.88" y1="83.82" x2="58.42" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="P$1"/>
<wire x1="58.42" y1="83.82" x2="73.66" y2="83.82" width="0.1524" layer="91"/>
<wire x1="60.96" y1="78.74" x2="58.42" y2="78.74" width="0.1524" layer="91"/>
<wire x1="58.42" y1="78.74" x2="58.42" y2="83.82" width="0.1524" layer="91"/>
<junction x="58.42" y="83.82"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IC1" gate="ADP1853" pin="ILIM"/>
<pinref part="R2" gate="G$1" pin="P$1"/>
<wire x1="55.88" y1="91.44" x2="60.96" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC1" gate="ADP1853" pin="DH"/>
<pinref part="M2" gate="G$1" pin="G"/>
<wire x1="55.88" y1="104.14" x2="73.66" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="P$1"/>
<pinref part="IC1" gate="ADP1853" pin="BST"/>
<wire x1="60.96" y1="101.6" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="P$2"/>
<wire x1="71.12" y1="101.6" x2="73.66" y2="101.6" width="0.1524" layer="91"/>
<wire x1="73.66" y1="101.6" x2="73.66" y2="96.52" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ADP1853" pin="SW"/>
<wire x1="73.66" y1="96.52" x2="55.88" y2="96.52" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ADP1853" pin="CS"/>
<wire x1="55.88" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<wire x1="73.66" y1="93.98" x2="73.66" y2="96.52" width="0.1524" layer="91"/>
<junction x="73.66" y="96.52"/>
<pinref part="R2" gate="G$1" pin="P$2"/>
<wire x1="71.12" y1="91.44" x2="73.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="73.66" y1="91.44" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<junction x="73.66" y="93.98"/>
<wire x1="73.66" y1="96.52" x2="78.74" y2="96.52" width="0.1524" layer="91"/>
<pinref part="M1" gate="G$1" pin="D"/>
<wire x1="78.74" y1="96.52" x2="78.74" y2="93.98" width="0.1524" layer="91"/>
<pinref part="M2" gate="G$1" pin="S"/>
<wire x1="78.74" y1="93.98" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<wire x1="78.74" y1="96.52" x2="78.74" y2="101.6" width="0.1524" layer="91"/>
<junction x="78.74" y="96.52"/>
<pinref part="L1" gate="G$1" pin="P$2"/>
<wire x1="88.9" y1="93.98" x2="78.74" y2="93.98" width="0.1524" layer="91"/>
<junction x="78.74" y="93.98"/>
</segment>
</net>
<net name="PGND" class="0">
<segment>
<pinref part="PGND1" gate="1" pin="PGND"/>
<pinref part="M1" gate="G$1" pin="S"/>
<wire x1="78.74" y1="81.28" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="P$2"/>
<wire x1="78.74" y1="78.74" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<wire x1="71.12" y1="78.74" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<junction x="78.74" y="78.74"/>
</segment>
<segment>
<pinref part="PGND2" gate="1" pin="PGND"/>
<pinref part="C5" gate="G$1" pin="P$1"/>
<wire x1="109.22" y1="58.42" x2="109.22" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="P$1"/>
<pinref part="PGND3" gate="1" pin="PGND"/>
<wire x1="116.84" y1="78.74" x2="116.84" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PGND4" gate="1" pin="PGND"/>
<pinref part="M3" gate="G$1" pin="S"/>
<wire x1="200.66" y1="81.28" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="P$2"/>
<wire x1="200.66" y1="78.74" x2="200.66" y2="58.42" width="0.1524" layer="91"/>
<wire x1="193.04" y1="78.74" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<junction x="200.66" y="78.74"/>
</segment>
<segment>
<pinref part="PGND5" gate="1" pin="PGND"/>
<pinref part="C11" gate="G$1" pin="P$1"/>
<wire x1="231.14" y1="58.42" x2="231.14" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="P$1"/>
<pinref part="PGND6" gate="1" pin="PGND"/>
<wire x1="238.76" y1="78.74" x2="238.76" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="IC1" gate="ADP1853" pin="FB"/>
<wire x1="55.88" y1="76.2" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="P$1"/>
<wire x1="101.6" y1="76.2" x2="101.6" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="P$2"/>
<wire x1="101.6" y1="78.74" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<junction x="101.6" y="76.2"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="P$2"/>
<pinref part="P+1" gate="VCC" pin="VBAT"/>
<wire x1="116.84" y1="88.9" x2="116.84" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="P$2"/>
<wire x1="116.84" y1="93.98" x2="116.84" y2="96.52" width="0.1524" layer="91"/>
<wire x1="109.22" y1="88.9" x2="109.22" y2="93.98" width="0.1524" layer="91"/>
<wire x1="109.22" y1="93.98" x2="116.84" y2="93.98" width="0.1524" layer="91"/>
<junction x="116.84" y="93.98"/>
<pinref part="L1" gate="G$1" pin="P$1"/>
<wire x1="109.22" y1="93.98" x2="101.6" y2="93.98" width="0.1524" layer="91"/>
<junction x="109.22" y="93.98"/>
<pinref part="R5" gate="G$1" pin="P$1"/>
<wire x1="101.6" y1="93.98" x2="99.06" y2="93.98" width="0.1524" layer="91"/>
<wire x1="101.6" y1="88.9" x2="101.6" y2="93.98" width="0.1524" layer="91"/>
<junction x="101.6" y="93.98"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="P$2"/>
<pinref part="P+4" gate="VCC" pin="VBAT"/>
<wire x1="238.76" y1="88.9" x2="238.76" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="P$2"/>
<wire x1="238.76" y1="93.98" x2="238.76" y2="96.52" width="0.1524" layer="91"/>
<wire x1="231.14" y1="88.9" x2="231.14" y2="93.98" width="0.1524" layer="91"/>
<wire x1="231.14" y1="93.98" x2="238.76" y2="93.98" width="0.1524" layer="91"/>
<junction x="238.76" y="93.98"/>
<pinref part="L2" gate="G$1" pin="P$1"/>
<wire x1="231.14" y1="93.98" x2="220.98" y2="93.98" width="0.1524" layer="91"/>
<junction x="231.14" y="93.98"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="P$1"/>
<wire x1="86.36" y1="111.76" x2="86.36" y2="114.3" width="0.1524" layer="91"/>
<wire x1="86.36" y1="114.3" x2="78.74" y2="114.3" width="0.1524" layer="91"/>
<pinref part="M2" gate="G$1" pin="D"/>
<wire x1="78.74" y1="114.3" x2="78.74" y2="111.76" width="0.1524" layer="91"/>
<wire x1="78.74" y1="114.3" x2="78.74" y2="116.84" width="0.1524" layer="91"/>
<junction x="78.74" y="114.3"/>
<pinref part="P+2" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="P$1"/>
<pinref part="P+3" gate="VCC" pin="VCC"/>
<wire x1="17.78" y1="104.14" x2="15.24" y2="104.14" width="0.1524" layer="91"/>
<wire x1="15.24" y1="104.14" x2="15.24" y2="116.84" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ADP1853" pin="ENB"/>
<wire x1="30.48" y1="101.6" x2="15.24" y2="101.6" width="0.1524" layer="91"/>
<wire x1="15.24" y1="101.6" x2="15.24" y2="104.14" width="0.1524" layer="91"/>
<junction x="15.24" y="104.14"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="P$1"/>
<wire x1="208.28" y1="111.76" x2="208.28" y2="114.3" width="0.1524" layer="91"/>
<wire x1="208.28" y1="114.3" x2="200.66" y2="114.3" width="0.1524" layer="91"/>
<pinref part="M4" gate="G$1" pin="D"/>
<wire x1="200.66" y1="114.3" x2="200.66" y2="111.76" width="0.1524" layer="91"/>
<wire x1="200.66" y1="114.3" x2="200.66" y2="116.84" width="0.1524" layer="91"/>
<junction x="200.66" y="114.3"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="P$1"/>
<pinref part="P+6" gate="VCC" pin="VCC"/>
<wire x1="139.7" y1="104.14" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<wire x1="137.16" y1="104.14" x2="137.16" y2="116.84" width="0.1524" layer="91"/>
<pinref part="IC2" gate="ADP1853" pin="ENB"/>
<wire x1="152.4" y1="101.6" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
<wire x1="137.16" y1="101.6" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<junction x="137.16" y="104.14"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="P$1"/>
<wire x1="55.88" y1="157.48" x2="55.88" y2="162.56" width="0.1524" layer="91"/>
<wire x1="55.88" y1="162.56" x2="48.26" y2="162.56" width="0.1524" layer="91"/>
<pinref part="IC1" gate="PWR" pin="VDD"/>
<wire x1="48.26" y1="162.56" x2="48.26" y2="160.02" width="0.1524" layer="91"/>
<pinref part="P+7" gate="VCC" pin="VCC"/>
<wire x1="48.26" y1="165.1" x2="48.26" y2="162.56" width="0.1524" layer="91"/>
<junction x="48.26" y="162.56"/>
</segment>
<segment>
<pinref part="IC2" gate="PWR" pin="VDD"/>
<pinref part="P+8" gate="VCC" pin="VCC"/>
<wire x1="73.66" y1="160.02" x2="73.66" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="P$1"/>
<wire x1="73.66" y1="162.56" x2="73.66" y2="165.1" width="0.1524" layer="91"/>
<wire x1="81.28" y1="157.48" x2="81.28" y2="162.56" width="0.1524" layer="91"/>
<wire x1="81.28" y1="162.56" x2="73.66" y2="162.56" width="0.1524" layer="91"/>
<junction x="73.66" y="162.56"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC2" gate="ADP1853" pin="RAMP"/>
<pinref part="R6" gate="G$1" pin="P$2"/>
<wire x1="152.4" y1="104.14" x2="149.86" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC2" gate="ADP1853" pin="TRK"/>
<wire x1="152.4" y1="99.06" x2="137.16" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="P$1"/>
<wire x1="137.16" y1="99.06" x2="137.16" y2="96.52" width="0.1524" layer="91"/>
<pinref part="IC2" gate="ADP1853" pin="VCCO"/>
<wire x1="137.16" y1="96.52" x2="137.16" y2="78.74" width="0.1524" layer="91"/>
<wire x1="152.4" y1="96.52" x2="144.78" y2="96.52" width="0.1524" layer="91"/>
<junction x="137.16" y="96.52"/>
<wire x1="144.78" y1="96.52" x2="137.16" y2="96.52" width="0.1524" layer="91"/>
<wire x1="144.78" y1="83.82" x2="144.78" y2="96.52" width="0.1524" layer="91"/>
<junction x="144.78" y="96.52"/>
<pinref part="IC2" gate="ADP1853" pin="FREQ"/>
<wire x1="152.4" y1="83.82" x2="144.78" y2="83.82" width="0.1524" layer="91"/>
<pinref part="IC2" gate="ADP1853" pin="FB"/>
<wire x1="177.8" y1="76.2" x2="185.42" y2="76.2" width="0.1524" layer="91"/>
<wire x1="185.42" y1="76.2" x2="185.42" y2="60.96" width="0.1524" layer="91"/>
<wire x1="185.42" y1="60.96" x2="132.08" y2="60.96" width="0.1524" layer="91"/>
<wire x1="132.08" y1="60.96" x2="132.08" y2="96.52" width="0.1524" layer="91"/>
<wire x1="132.08" y1="96.52" x2="137.16" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="IC2" gate="ADP1853" pin="DL"/>
<pinref part="M3" gate="G$1" pin="G"/>
<wire x1="177.8" y1="83.82" x2="180.34" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="P$1"/>
<wire x1="180.34" y1="83.82" x2="195.58" y2="83.82" width="0.1524" layer="91"/>
<wire x1="182.88" y1="78.74" x2="180.34" y2="78.74" width="0.1524" layer="91"/>
<wire x1="180.34" y1="78.74" x2="180.34" y2="83.82" width="0.1524" layer="91"/>
<junction x="180.34" y="83.82"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="IC2" gate="ADP1853" pin="ILIM"/>
<pinref part="R7" gate="G$1" pin="P$1"/>
<wire x1="177.8" y1="91.44" x2="182.88" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="IC2" gate="ADP1853" pin="DH"/>
<pinref part="M4" gate="G$1" pin="G"/>
<wire x1="177.8" y1="104.14" x2="195.58" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="C10" gate="G$1" pin="P$1"/>
<pinref part="IC2" gate="ADP1853" pin="BST"/>
<wire x1="182.88" y1="101.6" x2="177.8" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="C10" gate="G$1" pin="P$2"/>
<wire x1="193.04" y1="101.6" x2="195.58" y2="101.6" width="0.1524" layer="91"/>
<wire x1="195.58" y1="101.6" x2="195.58" y2="96.52" width="0.1524" layer="91"/>
<pinref part="IC2" gate="ADP1853" pin="SW"/>
<wire x1="195.58" y1="96.52" x2="177.8" y2="96.52" width="0.1524" layer="91"/>
<pinref part="IC2" gate="ADP1853" pin="CS"/>
<wire x1="177.8" y1="93.98" x2="195.58" y2="93.98" width="0.1524" layer="91"/>
<wire x1="195.58" y1="93.98" x2="195.58" y2="96.52" width="0.1524" layer="91"/>
<junction x="195.58" y="96.52"/>
<pinref part="R7" gate="G$1" pin="P$2"/>
<wire x1="193.04" y1="91.44" x2="195.58" y2="91.44" width="0.1524" layer="91"/>
<wire x1="195.58" y1="91.44" x2="195.58" y2="93.98" width="0.1524" layer="91"/>
<junction x="195.58" y="93.98"/>
<wire x1="195.58" y1="96.52" x2="200.66" y2="96.52" width="0.1524" layer="91"/>
<pinref part="M3" gate="G$1" pin="D"/>
<wire x1="200.66" y1="96.52" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<pinref part="M4" gate="G$1" pin="S"/>
<wire x1="200.66" y1="93.98" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="200.66" y1="96.52" x2="200.66" y2="101.6" width="0.1524" layer="91"/>
<junction x="200.66" y="96.52"/>
<pinref part="L2" gate="G$1" pin="P$2"/>
<wire x1="210.82" y1="93.98" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<junction x="200.66" y="93.98"/>
</segment>
</net>
<net name="SS" class="0">
<segment>
<pinref part="IC1" gate="ADP1853" pin="SS"/>
<wire x1="30.48" y1="81.28" x2="27.94" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="P$1"/>
<wire x1="27.94" y1="81.28" x2="22.86" y2="81.28" width="0.1524" layer="91"/>
<wire x1="22.86" y1="81.28" x2="22.86" y2="78.74" width="0.1524" layer="91"/>
<wire x1="27.94" y1="43.18" x2="27.94" y2="81.28" width="0.1524" layer="91"/>
<junction x="27.94" y="81.28"/>
</segment>
<segment>
<pinref part="IC2" gate="ADP1853" pin="SS"/>
<wire x1="152.4" y1="81.28" x2="144.78" y2="81.28" width="0.1524" layer="91"/>
<wire x1="144.78" y1="43.18" x2="144.78" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="IC1" gate="ADP1853" pin="CLKOUT"/>
<wire x1="58.42" y1="43.18" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<wire x1="58.42" y1="66.04" x2="55.88" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="ADP1853" pin="SYNC"/>
<wire x1="152.4" y1="88.9" x2="149.86" y2="88.9" width="0.1524" layer="91"/>
<wire x1="149.86" y1="88.9" x2="149.86" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PGOOD" class="0">
<segment>
<pinref part="IC1" gate="ADP1853" pin="PGOOD"/>
<wire x1="60.96" y1="43.18" x2="60.96" y2="68.58" width="0.1524" layer="91"/>
<wire x1="60.96" y1="68.58" x2="55.88" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="ADP1853" pin="PGOOD"/>
<wire x1="177.8" y1="68.58" x2="180.34" y2="68.58" width="0.1524" layer="91"/>
<wire x1="180.34" y1="68.58" x2="180.34" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="COMP" class="0">
<segment>
<pinref part="IC1" gate="ADP1853" pin="COMP"/>
<wire x1="63.5" y1="43.18" x2="63.5" y2="73.66" width="0.1524" layer="91"/>
<wire x1="63.5" y1="73.66" x2="55.88" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="ADP1853" pin="COMP"/>
<wire x1="177.8" y1="73.66" x2="182.88" y2="73.66" width="0.1524" layer="91"/>
<wire x1="182.88" y1="73.66" x2="182.88" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
