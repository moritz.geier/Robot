.ALIASES
X_U1            U1(VREG=VREG PWPD=0 EN=N15001 SW=N15016 VO=VOUT VBST=N15005 DRVH=N15052 DRVL=N15056 PGOOD=N14997 VDD=N15226
+VFB=N15283 TRIP=N15329 GND=0 ) CN @CONSTANT CURRENT
+SOURCE.SCHEMATIC1(sch_1):INS14339@TPS53819A_TRANS.TPS53819A_TRANS.Normal(chips)
V_V1            V1(+=VIN -=0 ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14448@SOURCE.VSRC.Normal(chips)
R_R1            R1(1=N15001 2=VREG ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14537@ANALOG.R.Normal(chips)
R_R2            R2(1=N14997 2=VREG ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14553@ANALOG.R.Normal(chips)
R_R3            R3(1=0 2=N15329 ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14569@ANALOG.R.Normal(chips)
R_R4            R4(1=VOUT 2=N15283 ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14594@ANALOG.R.Normal(chips)
R_R5            R5(1=0 2=N15283 ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14610@ANALOG.R.Normal(chips)
R_R6            R6(1=VIN 2=N15226 ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14626@ANALOG.R.Normal(chips)
R_R7            R7(1=N15040 2=N15052 ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14642@ANALOG.R.Normal(chips)
R_R8            R8(1=N15016 2=N15012 ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14658@ANALOG.R.Normal(chips)
C_C3            C3(1=N15005 2=N15012 ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14683@ANALOG.C.Normal(chips)
C_C4            C4(1=0 2=VREG ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14699@ANALOG.C.Normal(chips)
C_C5            C5(1=0 2=N15226 ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14715@ANALOG.C.Normal(chips)
X_U2            U2(Vin1=VIN Vin2=VIN TG=N15040 TGR=N15016 BG=N15056 Vsw1=N15081 Vsw2=N15081 Vsw3=N15081 Pgnd=0 ) CN @CONSTANT
+CURRENT SOURCE.SCHEMATIC1(sch_1):INS14754@CSD87350Q5D.csd87350q5d.Normal(chips)
C_C6            C6(1=0 2=VIN ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14793@ANALOG.C.Normal(chips)
C_C7            C7(1=N15142 2=N15081 ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14809@ANALOG.C.Normal(chips)
R_R9            R9(1=0 2=N15142 ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14834@ANALOG.R.Normal(chips)
L_L1            L1(1=N15081 2=VOUT ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS14859@ANALOG.L.Normal(chips)
R_R10           R10(1=VOUT 2=0 ) CN @CONSTANT CURRENT SOURCE.SCHEMATIC1(sch_1):INS15623@ANALOG.R.Normal(chips)
_    _(VIN=VIN)
_    _(VOUT=VOUT)
_    _(VREG=VREG)
.ENDALIASES
