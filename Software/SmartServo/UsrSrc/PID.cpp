// ------------------------------------ //
//
// Author		: Moritz Geier
// Date			: 15.01.2020
// Project Name	: SmartServo
// File Name	: PID
// Version		: 1
// Description	: This program calculates
//				  a PID controller
//
// ------------------------------------ //

#include "PID.h"

double PIDperiod;

PID::PID(double p_bias, double i_bias, double d_bias) : p_bias(p_bias), i_bias(i_bias), d_bias(d_bias)
{
	_max = std::numeric_limits<double>::max();
	_min = _max * -1;
}

double PID::Calculate(double setPoint, double currentPoint, double samplePeriod)
{
	double error = setPoint - currentPoint;
	double pidValue = 0;

	this->samplePeriod = samplePeriod;

	if(this->p_bias != 0)
		pidValue += Calc_P(error);


	if(this->i_bias != 0 && error != 0)
		pidValue += Calc_I(error);


	if(this->d_bias != 0)
		pidValue += Calc_D(error);

	if(pidValue < this->_min)
		pidValue = this->_min;

	if(pidValue > this->_max)
		pidValue = this->_max;

	return pidValue;
}

// Represents the present error
double PID::Calc_P(double error)
{
	return p_bias * error;
}

// Interpretation of the past
double PID::Calc_I(double error)
{
	// I(tk+1) = I(tk) + (kp*h)/Ti*e(tk)
	// I(x) = function of integral
	// tk = time point at calculation
	// kp = p_bias
	// e(x) = error function
	// Ti = i_bias
	// h = sampling periods
	this->i  += this->i_bias*this->samplePeriod*error;

	// To prevent windup
	if(this->i < this->_min)
		this->i = this->_min;

	if(this->i > this->_max)
			this->i = this->_max;


	return this->i;
}

// Predicts the future
double PID::Calc_D(double error)
{
	// D(tk) = Td/(Td + h) * D(tk−1) − kp/(Td + h) * (y(tk) − y(tk−1))
	// D(x) = function of derivative
	// tk = time point at calculation
	// kd = d_bias
	// y(x) = error function
	// Td = d_bias
	// h = sampling period
	this->d = this->d_bias*(error - this->lastError)/this->samplePeriod;
	this->lastError = error;
	return this->d;
}

void PID::Tune(double p, double i, double d)
{
	this->p_bias = p;
	this->i_bias = i;
	this->d_bias = d;

	this->i = 0;
}
