// ------------------------------------ //
//
// Author		: Moritz Geier
// Date			: 09.02.2020
// Project Name	: SmartServo
// File Name	: Sensors
// Version		: 1
// Description	: This program reads all
//				  Sensors and converts them
//				  into readable data
//
// ------------------------------------ //

#include "Sensors.h"

Sensors::Sensors()
{
	current1.factor = 1;
	current1.offset = 0;

	current2.factor = 1;
	current2.offset = 0;

	internalVoltage.factor = 1;
	internalVoltage.offset = 0;

	temp.factor = 1;
	temp.offset = 0;
}

void Sensors::GatherData()
{
	this->fault = HAL_GPIO_ReadPin(nFAULT_GPIO_Port, nFAULT_Pin);
	Calculate(adc_values[ISENS1_ADC_INDEX],&current1);
	Calculate(adc_values[ISENS2_ADC_INDEX],&current2);
	Calculate(adc_values[INT_VOLT_ADC_INDEX],&internalVoltage);
	Calculate(adc_values[TEMP_ADC_INDEX],&temp);
}

void Sensors::Calculate(uint16_t value, Sensor *sensor)
{
	sensor->value = value*sensor->factor+sensor->offset;
}
