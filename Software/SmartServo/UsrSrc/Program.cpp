// ------------------------------------ //
//
// Author		: Moritz Geier
// Date			: 11.01.2020
// Project Name	: SmartServo
// File Name	: Program
// Version		: 1
// Description	: This program is a interface
//				  between the low level C API
//				  and the project C++ code
//
// ------------------------------------ //


#include "Program.h"
#include "Servo.h"
#include "Sensors.h"

extern "C" void Program();

#ifdef DEBUG_MODE
int motorState = 2;
double pos_p=1,pos_i=0,pos_d=0,vel_p=1,vel_i=0,vel_d=0;
double filterAplha;
uint16_t setPoint = 512;
#endif

void Program()
{
	Servo servo;
	Sensors sensors;

	StartHardWare();

	servo.SetAngle(512);

	//HAL_Delay(5000);

	while(true)
	{
		if(update)
		{
#ifdef DEBUG_MODE
			servo.SetAngle(setPoint);
			switch (motorState) {
				case 0:
#endif

					if(HAL_GPIO_ReadPin(nFAULT_GPIO_Port, nFAULT_Pin) != GPIO_PIN_RESET)
					{
						servo.Update();
						update = false;
					}
					else
					{

					}

#ifdef DEBUG_MODE
					break;
				case 1:
					servo.Stop();
					break;
				case 2:
					servo.TunePosPID(pos_p, pos_i, pos_d);
					servo.TuneVelPID(vel_p, vel_i, vel_d);
					servo.TuneFilter(filterAplha);
					break;
			}
#endif
		}
	}
}
